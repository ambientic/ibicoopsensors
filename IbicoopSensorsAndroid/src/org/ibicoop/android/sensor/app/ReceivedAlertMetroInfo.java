package org.ibicoop.android.sensor.app;

public class ReceivedAlertMetroInfo extends MetroInfo {

	private String mCurrentDateAndTime;
	private float[] mAcceleration = {0f, 0f, 0f};
	
	public ReceivedAlertMetroInfo(long paramId, double paramLongitude, double paramLatitude, 
			String paramName, String paramArea, String paramTransport, String paramCurrentDateAndTime) {
		super(paramId, paramLongitude, paramLatitude, paramName, paramArea, paramTransport);
		mCurrentDateAndTime = paramCurrentDateAndTime;
	}
	
	public ReceivedAlertMetroInfo(long paramId, double paramLongitude, double paramLatitude, 
			String paramName, String paramArea, String paramTransport, 
			String paramCurrentDateAndTime, float[] paramAcceleration) {

		this(paramId, paramLongitude, paramLatitude, 
				paramName, paramArea, paramTransport, paramCurrentDateAndTime);
		mAcceleration = paramAcceleration;
	}
	
	public String getCurrentDateAndTime() {
		return mCurrentDateAndTime;
	}
	
	public float[] getAcceleration() {
		return mAcceleration;
	}
	
	public String toString() {
		String info = "ReceivedAlertMetroInfo : Id = " + mId 
				+ ", longitude = " + mLongitude 
				+ ", latitude = " + mLatitude
				+ ", name = " + mName
				+ ", area = " + mArea
				+ ", transport = " + mTransport
				+ ", timestamp = " + mCurrentDateAndTime
				+ ", accelX = " + mAcceleration[0] 
				+ ", accelY = " + mAcceleration[1]
				+ ", accelZ = " + mAcceleration[2];
		return info;
	}
	
	public String[] toArrayString() {
		String[] data = {String.valueOf(mId), String.valueOf(mLongitude), String.valueOf(mLatitude),
				String.valueOf(mName), String.valueOf(mArea), String.valueOf(mTransport), 
				String.valueOf(mCurrentDateAndTime), String.valueOf(mAcceleration[0])
				, String.valueOf(mAcceleration[1]), String.valueOf(mAcceleration[2])};
		return data;
	}
}
