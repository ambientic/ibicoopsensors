package org.ibicoop.android.sensor.app;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.ibicoop.sensor.IbiSensor;
import org.ibicoop.sensor.IbiSensorFactory;
import org.ibicoop.sensor.IbiSensorListener;
import org.ibicoop.sensor.IbiSensorType;
import org.ibicoop.sensor.data.IbiSensorData;
import org.ibicoop.sensor.data.IbiSensorDataBluetoothList;
import org.ibicoop.sensor.data.IbiSensorDataBoolean;
import org.ibicoop.sensor.data.IbiSensorDataFloat3D;
import org.ibicoop.sensor.data.IbiSensorDataLocation;
import org.ibicoop.sensor.data.IbiSensorDataMicrophone;
import org.ibicoop.sensor.exceptions.IbiSensorException;
import org.ibicoop.sensor.resolution.IbiSensorResolutionLocation;
import org.ibicoop.sensor.resolution.IbiSensorResolutionLocationAccuracyType;
import org.ibicoop.sensor.resolution.IbiSensorResolutionMicrophone;
import org.ibicoop.sensor.resolution.IbiSensorResolutionTime;
import org.ibicoop.sensor.trigger.IbiSensorDataInRegionSystemTrigger;
import org.ibicoop.sensor.trigger.IbiSensorDataInRegionSystemTriggerListener;
import org.ibicoop.sensor.trigger.IbiSensorDataMetroStopTrigger;
import org.ibicoop.sensor.trigger.IbiSensorDataMetroStopTriggerListener;
import org.ibicoop.sensor.trigger.IbiSensorDataTimerNotAdaptativeInRangeTrigger;
import org.ibicoop.sensor.trigger.IbiSensorDataTimerNotAdaptativeLocationInRegionTrigger;
import org.ibicoop.sensor.trigger.IbiSensorDataTimerNotAdaptativeNewDataTrigger;
import org.ibicoop.sensor.trigger.IbiSensorDataTimerNotAdaptativeOverRangeTrigger;
import org.ibicoop.sensor.trigger.IbiSensorDataTimerNotAdaptativeUnderRangeTrigger;
import org.ibicoop.sensor.trigger.IbiSensorDataTrigger;
import org.ibicoop.sensor.trigger.IbiSensorDataTriggerListener;
import org.ibicoop.sensor.unit.IbiSensorUnit;
import org.ibicoop.sensor.unit.IbiSensorUnitLocation;
import org.ibicoop.sensor.unit.IbiSensorUnitType;
import org.ibicoop.android.statespace.MetroModel;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import au.com.bytecode.opencsv.CSVWriter;

public class MainActivity extends Activity {

        private static final String FOLDER_NAME = "/MyAccelerationTrace";
        private static final String VARIABLE_CSV_FILE_NAME_HEADER = "/myAccelerationTrace";
        private static final String CSV_FILE_NAME_EXTENSION = ".csv";
        private static final boolean DEBUG = false;
        private static final long ALERT_ACTIVE_TIME_MS = 1000 * 10; //10 minutes

        // Ibisensor
        private IbiSensor accelerometer;
        private IbiSensor locationSensor;
        private IbiSensor bluetoothSensor;
        private IbiSensor soundSensor;
        private IbiSensor metroSensor;

        // IbiSensorResolution
        private IbiSensorResolutionLocation resolutionLocation;

        // IbiSensorDataTimer
        private IbiSensorDataTimerNotAdaptativeOverRangeTrigger trigger;
        private IbiSensorDataTimerNotAdaptativeUnderRangeTrigger triggerUnder;
        private IbiSensorDataTimerNotAdaptativeInRangeTrigger triggerInRange;
        private IbiSensorDataTimerNotAdaptativeLocationInRegionTrigger triggerInRegion;
        private IbiSensorDataTimerNotAdaptativeNewDataTrigger triggerNewData;
        private IbiSensorDataTimerNotAdaptativeNewDataTrigger triggerNewDataForAccel;
        private IbiSensorDataTimerNotAdaptativeNewDataTrigger triggerNewDataForBluetooth;
        private IbiSensorDataInRegionSystemTrigger sysInRegionTrigger;
        private IbiSensorDataMetroStopTrigger metroStopTrigger;

        private TextView tvName, tvLat, tvLon, tvTrigger, tvInRegion, tvBT;
        private TextView tvRecord, tvTransport, tvMove, tvHuman, tvMobile, tvPocket, tvAmplitude, tvFrequency;
        private TextView tvSimuMove, tvSameState, tvOutputState, tvMetroSensor;
        private Spinner spTransport, spMove, spHuman, spMobile, spPocket;
        private String stTransport, stMove, stHuman, stMobile, stPocket;
        private Button btnStartStop, btnMoveStop;
        private Button btnResetState;

        private boolean isStarted = false;
        private boolean isMoving = false;

        private static int countTrigger = 0;
        private static int inRegionCounter = 0;

        private static File folder;
        private static CSVWriter writer;

        private MetroInfoListGetter metroInfoListGetter;
        private List<MetroInfo> metroInfoList;

        private float[] accelerations = { 0f, 0f, 0f };
        private double amplitude = 0.0;
        private double decibel = 0.0;
        private float frequency = 0f;

        private MetroModel model;
        private int simuMove = 0;
        private int moveValue = 0;
        private double currentOutput = 0.0;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                setContentView(R.layout.activity_main);

                model = new MetroModel();

                stMove = "Stop";
                stTransport = "metro";

                tvMetroSensor = (TextView) findViewById(R.id.tvMetroSensor);

                tvSimuMove = (TextView) findViewById(R.id.tvMoveSimu);
                tvSameState = (TextView) findViewById(R.id.tvSameState);
                tvOutputState = (TextView) findViewById(R.id.tvOutputState);
                btnResetState = (Button) findViewById(R.id.buttonResetState);
                btnResetState.setOnClickListener(resetClickListener);

                tvName = (TextView) findViewById(R.id.stationName);
                tvLat = (TextView) findViewById(R.id.latitude);
                tvLon = (TextView) findViewById(R.id.longitude);
                tvTrigger = (TextView) findViewById(R.id.trigger);
                tvInRegion = (TextView) findViewById(R.id.inRegion);
                tvBT = (TextView) findViewById(R.id.bluetooth);
                tvLat.setText("0.0");
                tvLon.setText("0.0");
                tvTrigger.setText(String.valueOf(countTrigger));
                tvInRegion.setText(String.valueOf(inRegionCounter));
                tvRecord = (TextView) findViewById(R.id.tvRecordMode);
                tvTransport = (TextView) findViewById(R.id.tvTransportMode);
                tvMove = (TextView) findViewById(R.id.tvMoveMode);
                tvHuman = (TextView) findViewById(R.id.tvHumanMode);
                tvMobile = (TextView) findViewById(R.id.tvMobileMode);
                tvPocket = (TextView) findViewById(R.id.tvPocketMode);
                tvAmplitude = (TextView) findViewById(R.id.soundAmplitude);
                tvFrequency = (TextView) findViewById(R.id.soundFrequency);

                btnStartStop = (Button) findViewById(R.id.buttonRecord);
                btnStartStop.setOnClickListener(startStopClickListener);
                btnMoveStop = (Button) findViewById(R.id.buttonMove);
                btnMoveStop.setOnClickListener(moveStopClickListener);

                // Spinners
                spTransport = (Spinner) findViewById(R.id.transportSpinner);
                ArrayAdapter<CharSequence> adapterTransport = ArrayAdapter.createFromResource(this, R.array.metroName, android.R.layout.simple_spinner_item);
                adapterTransport.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spTransport.setAdapter(adapterTransport);
                spTransport.setOnItemSelectedListener(new OnItemSelectedListener() {

                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                                stTransport = parent.getItemAtPosition(pos).toString();
                                tvTransport.setText(stTransport);
                                if (DEBUG)
                                        System.out.println(stTransport);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> arg0) {
                        }
                });

                spMove = (Spinner) findViewById(R.id.moveSpinner);
                ArrayAdapter<CharSequence> adapterMove = ArrayAdapter.createFromResource(this, R.array.moveMode, android.R.layout.simple_spinner_item);
                adapterMove.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spMove.setAdapter(adapterMove);
                spMove.setOnItemSelectedListener(new OnItemSelectedListener() {

                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                                stMove = parent.getItemAtPosition(pos).toString();
                                tvMove.setText(stMove);
                                if (DEBUG)
                                        System.out.println(stMove);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> arg0) {
                        }
                });

                spHuman = (Spinner) findViewById(R.id.humanSpinner);
                ArrayAdapter<CharSequence> adapterHuman = ArrayAdapter.createFromResource(this, R.array.humanMode, android.R.layout.simple_spinner_item);
                adapterHuman.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spHuman.setAdapter(adapterHuman);
                spHuman.setOnItemSelectedListener(new OnItemSelectedListener() {

                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                                stHuman = parent.getItemAtPosition(pos).toString();
                                tvHuman.setText(stHuman);
                                if (DEBUG)
                                        System.out.println(stHuman);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> arg0) {
                        }
                });

                spMobile = (Spinner) findViewById(R.id.mobileSpinner);
                ArrayAdapter<CharSequence> adapterMobile = ArrayAdapter.createFromResource(this, R.array.mobileMode, android.R.layout.simple_spinner_item);
                adapterMobile.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spMobile.setAdapter(adapterMobile);
                spMobile.setOnItemSelectedListener(new OnItemSelectedListener() {

                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                                stMobile = parent.getItemAtPosition(pos).toString();
                                tvMobile.setText(stMobile);
                                if (DEBUG)
                                        System.out.println(stMobile);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> arg0) {
                        }
                });

                spPocket = (Spinner) findViewById(R.id.pocketSpinner);
                ArrayAdapter<CharSequence> adapterPocket = ArrayAdapter.createFromResource(this, R.array.pocketMode, android.R.layout.simple_spinner_item);
                adapterPocket.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spPocket.setAdapter(adapterPocket);
                spPocket.setOnItemSelectedListener(new OnItemSelectedListener() {

                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                                stPocket = parent.getItemAtPosition(pos).toString();
                                tvPocket.setText(stPocket);
                                if (DEBUG)
                                        System.out.println(stPocket);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> arg0) {
                        }
                });

                long period = 1000; // ms

                try {
                        soundSensor = IbiSensorFactory.getInstance().getSensor(MainActivity.this, IbiSensorType.MICROPHONE, new IbiSensorResolutionMicrophone(44100, period), listener);

                        bluetoothSensor = IbiSensorFactory.getInstance().getSensor(MainActivity.this, IbiSensorType.BLUETOOTH, new IbiSensorResolutionTime(20), listener);

                        accelerometer = IbiSensorFactory.getInstance().getSensor(MainActivity.this, IbiSensorType.ACCELEROMETER, new IbiSensorResolutionTime(20), listener);
                        IbiSensorData threshold1 = new IbiSensorDataFloat3D(-0.8f, 0f, 9f, 0l, new IbiSensorUnit(IbiSensorUnitType.ACCELERATION_UNIT));

                        triggerNewDataForAccel = new IbiSensorDataTimerNotAdaptativeNewDataTrigger(accelerometer, threshold1, triggerListener, period, -1);
                        resolutionLocation = new IbiSensorResolutionLocation(IbiSensorResolutionLocationAccuracyType.ACCURACY_FINE, 0, 0f);

                        // Launch location sensor
                        locationSensor = IbiSensorFactory.getInstance().getSensor(MainActivity.this, IbiSensorType.LOCATION, resolutionLocation, listener);

                        // Initialize system in region listener
                        sysInRegionTrigger = IbiSensorDataInRegionSystemTrigger.getInstance();
                        sysInRegionTrigger.init(locationSensor, ALERT_ACTIVE_TIME_MS, inRegionListener);

                        // Create metro sensor
                        //metroSensor = IbiSensorFactory.getInstance().getSensor(MainActivity.this, IbiSensorType.METRO, null, listener);

                        // Initialize metro stop listener
                        //metroStopTrigger = IbiSensorDataMetroStopTrigger.getInstance();
                        //metroStopTrigger.init(metroSensor, metroStopListener, 0);

                } catch (IbiSensorException e) {
                }

                new MetrosTask().execute();

                triggerNewDataForBluetooth = new IbiSensorDataTimerNotAdaptativeNewDataTrigger(bluetoothSensor, new IbiSensorDataBluetoothList(System.currentTimeMillis()), bluetoothTriggerListener, period, -1);
        }

        @Override
        public void onDestroy() {
                System.out.println("On destroy");

                // Stop the write file
                isStarted = false;

                try {

                        if (metroSensor != null)
                                metroSensor.stop();
                        if (soundSensor != null)
                                soundSensor.stop();
                        if (bluetoothSensor != null)
                                bluetoothSensor.stop();
                        if (accelerometer != null)
                                accelerometer.stop();
                        if (locationSensor != null)
                                locationSensor.stop();
                        if (trigger != null)
                                trigger.stop();
                        if (triggerUnder != null)
                                triggerUnder.stop();
                        if (triggerInRange != null)
                                triggerInRange.stop();
                        if (triggerInRegion != null)
                                triggerInRegion.stop();
                        if (triggerNewData != null)
                                triggerNewData.stop();
                        if (triggerNewDataForAccel != null)
                                triggerNewDataForAccel.stop();
                        if (triggerNewDataForBluetooth != null)
                                triggerNewDataForBluetooth.stop();

                } catch (IbiSensorException e1) {
                }

                try {
                        if (writer != null)
                                writer.close();
                } catch (IOException e) {
                        e.printStackTrace();
                }

                super.onDestroy();
        }

        class MetrosTask extends AsyncTask<Void, Void, Void> {

                @Override
                protected Void doInBackground(Void... params) {
                        metroInfoListGetter = new MetroInfoListGetter(MainActivity.this, "ratp_arret_graphique.csv");

                        MetroInfo ambientic = new MetroInfo(1, 2.10245, 48.83772, "INRIA Ambientic", "Rocquencourt", "navette");

                        metroInfoListGetter.addMetroInfoInList(ambientic);

                        MetroInfo ambientic2 = new MetroInfo(1, 2.10245, 48.83772, "INRIA Ambientic", "Rocquencourt", "navette");

                        metroInfoListGetter.addMetroInfoInList(ambientic2);

                        MetroInfo ambientic3 = new MetroInfo(2, 2.10297, 48.83765, "Main building", "Rocquencourt", "navette");

                        metroInfoListGetter.addMetroInfoInList(ambientic3);

                        MetroInfo canteen = new MetroInfo(1, 2.10106, 48.83701, "INRIA canteen", "Rocquencourt", "navette");

                        metroInfoListGetter.addMetroInfoInList(canteen);

                        MetroInfo imara = new MetroInfo(1, 2.10089, 48.83782, "INRIA imara", "Rocquencourt", "navette");

                        metroInfoListGetter.addMetroInfoInList(imara);

                        MetroInfo entrance = new MetroInfo(1, 2.10332, 48.83703, "INRIA entrance", "Rocquencourt", "navette");

                        metroInfoListGetter.addMetroInfoInList(entrance);

                        MetroInfo field1 = new MetroInfo(1, 2.09976, 48.83709, "INRIA field1", "Rocquencourt", "navette");

                        metroInfoListGetter.addMetroInfoInList(field1);

                        MetroInfo field2 = new MetroInfo(1, 2.09889, 48.83696, "INRIA field2", "Rocquencourt", "navette");

                        metroInfoListGetter.addMetroInfoInList(field2);

                        MetroInfo field3 = new MetroInfo(1, 2.09803, 48.83694, "INRIA field3", "Rocquencourt", "navette");

                        metroInfoListGetter.addMetroInfoInList(field3);

                        MetroInfo field4 = new MetroInfo(1, 2.10017, 48.83652, "INRIA field4", "Rocquencourt", "navette");

                        metroInfoListGetter.addMetroInfoInList(field4);

                        MetroInfo field5 = new MetroInfo(1, 2.09890, 48.83634, "INRIA field5", "Rocquencourt", "navette");

                        metroInfoListGetter.addMetroInfoInList(field5);

                        MetroInfo field6 = new MetroInfo(1, 2.09769, 48.83627, "INRIA field6", "Rocquencourt", "navette");

                        metroInfoListGetter.addMetroInfoInList(field6);

                        metroInfoList = metroInfoListGetter.getMetroInfoList();

                        for (MetroInfo info : metroInfoList) {

                                Map<String, String> infoMap = new HashMap<String, String>();
                                infoMap.put("name", info.getName());

                                IbiSensorDataLocation ibiSensorData = new IbiSensorDataLocation(info.getLatitude(), info.getLongitude(), 0.0, 0.0f, 0.0f, System.currentTimeMillis(), 100.0f, infoMap,
                                                new IbiSensorUnitLocation());

                                sysInRegionTrigger.addThreshold(ibiSensorData);
                        }

                        return null;
                }

        }

        private OnClickListener resetClickListener = new OnClickListener() {
                @Override
                public void onClick(View v) {
                        if (model != null)
                                model.resetState();
                }
        };

        private OnClickListener startStopClickListener = new OnClickListener() {

                @Override
                public void onClick(View v) {
                        isStarted = !isStarted;
                        if (isStarted) {
                                tvRecord.setText("Start recording");
                                btnStartStop.setText("Stop recording");
                        } else {
                                tvRecord.setText("Stop recording");
                                btnStartStop.setText("Start recording");

                                if (writer != null) {

                                        try {
                                                writer.close();
                                        } catch (Exception exception) {
                                                exception.printStackTrace();
                                        }

                                        writer = null;
                                }
                        }
                }
        };

        private OnClickListener moveStopClickListener = new OnClickListener() {

                @Override
                public void onClick(View v) {
                        isMoving = !isMoving;
                        if (isMoving) {
                                tvMove.setText("Metro moving");
                                btnMoveStop.setText("Metro stopping");
                                stMove = "Moving";
                        } else {
                                tvMove.setText("Metro stopping");
                                btnMoveStop.setText("Metro moving");
                                stMove = "Stop";
                        }
                }
        };

        private IbiSensorListener listener = new IbiSensorListener() {

                @Override
                public void stopped(IbiSensor ibiSensor) {
                        if (ibiSensor.equals(accelerometer)) {
                                if (DEBUG)
                                        System.out.println("Accelerometer is disconnected");
                        } else if (ibiSensor.equals(locationSensor)) {
                                if (DEBUG)
                                        System.out.println("Location sensor is disconnected");
                        }
                }

                @Override
                public void started(IbiSensor ibiSensor) {
                        if (ibiSensor.equals(accelerometer)) {
                                if (DEBUG)
                                        System.out.println("1) Accelerometer is connected");
                        } else if (ibiSensor.equals(locationSensor)) {
                                if (DEBUG)
                                        System.out.println("Location sensor is connected");
                        }
                }

                @Override
                public void paused(IbiSensor ibiSensor) {
                        // TODO Auto-generated method stub

                }

                @Override
                public void resumed(IbiSensor ibiSensor) {
                        // TODO Auto-generated method stub

                }

        };

        private IbiSensorDataInRegionSystemTriggerListener inRegionListener = new IbiSensorDataInRegionSystemTriggerListener() {

                @Override
                public void ibiSensorDataInRegionTrigger(IbiSensor ibiSensor, IbiSensorDataInRegionSystemTrigger dataTrigger, final IbiSensorDataLocation ibiSensorData) {

                        new Thread() {

                                public void run() {
                                        runOnUiThread(new Runnable() {

                                                @Override
                                                public void run() {
                                                        IbiSensorDataLocation locData = ((IbiSensorDataLocation) ibiSensorData);

                                                        if (DEBUG)
                                                                System.out.println("IbiSensorData : " + locData.toString());

                                                        Map<String, String> parameters = locData.getExtraParameters();

                                                        if (parameters != null) {

                                                                System.out.println("Parameters is not null");

                                                                if (parameters.get("name") != null) {

                                                                        System.out.println("Name is not null");

                                                                        tvName.setText(parameters.get("name"));
                                                                }
                                                        }

                                                        inRegionCounter++;
                                                        if (DEBUG)
                                                                System.out.println("In region counter : " + inRegionCounter);
                                                        tvInRegion.setText(String.valueOf(inRegionCounter));
                                                        tvLat.setText(String.valueOf(locData.getLatitude()));
                                                        tvLon.setText(String.valueOf(locData.getLongitude()));

                                                }
                                        });
                                }

                        }.start();
                }

				@Override
				public void ibiSensorDataOutOfRegionTrigger(
						IbiSensor ibiSensor,
						IbiSensorDataInRegionSystemTrigger dataTrigger,
						IbiSensorDataLocation ibiSensorData) {
					System.out.println("Out of region!");
				}

        };

        private IbiSensorDataMetroStopTriggerListener metroStopListener = new IbiSensorDataMetroStopTriggerListener() {

                @Override
                public void ibiSensorDataMetroStopTrigger(IbiSensor ibiSensor, IbiSensorDataMetroStopTrigger dataTrigger, final IbiSensorDataBoolean ibiSensorData) {

                        new Thread() {

                                public void run() {
                                        runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                        System.out.println("IbiSensorData : " + ibiSensorData.toString());
                                                        long timestamp = ibiSensorData.getTimeStamp();
                                                        boolean movingState = ((Boolean) ibiSensorData.getValue());
                                                        String movingStateDesc = "";

                                                        if (movingState) {
                                                                movingStateDesc = "Move";
                                                        } else {
                                                                movingStateDesc = "Stop";
                                                        }

                                                        // Prepare date format
                                                        String format = "ddMMyyyy_HHmmss";
                                                        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.FRANCE);
                                                        String currentDateandTime = sdf.format(new Date(timestamp));

                                                }
                                        });
                                }

                        }.start();
                }

        };

        private IbiSensorDataTriggerListener bluetoothTriggerListener = new IbiSensorDataTriggerListener() {

                @Override
                public void ibiSensorDataTrigger(IbiSensor ibiSensor, IbiSensorDataTrigger dataTrigger, final IbiSensorData ibiSensorData) {

                        new Thread() {
                                public void run() {
                                        runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                        System.out.println("#####Bluetooth data trigger#####");
                                                        System.out.println(ibiSensorData.toString());
                                                        tvBT.setText(ibiSensorData.toString());
                                                }
                                        });
                                }
                        }.start();

                }

        };

        private IbiSensorDataTriggerListener triggerListener = new IbiSensorDataTriggerListener() {

                @Override
                public void ibiSensorDataTrigger(IbiSensor ibiSensor, IbiSensorDataTrigger dataTrigger, IbiSensorData ibiSensorData) {

                        accelerations = (float[]) ibiSensorData.getValue();

                        // Get microphone sound data
                        IbiSensorDataMicrophone microphoneData;
                        try {
                                microphoneData = (IbiSensorDataMicrophone) soundSensor.getData();
                                amplitude = microphoneData.getAmplitude();
                                decibel = microphoneData.getDecibel();
                                frequency = microphoneData.getFrequency();

                                // Update model state
                                model.updateState(accelerations, amplitude, frequency);

                                currentOutput = model.getCurrentOutput();
                                simuMove = model.getProcessedOutput();

                                if (isStarted) {
                                        if (DEBUG)
                                                System.out.println("write csv file");
                                        writeCSVFile(valuesToStringArray(accelerations));
                                }

                                runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {

                                                tvOutputState.setText("" + currentOutput);

                                                moveValue = 0;
                                                if (stMove.equals("Moving"))
                                                        moveValue = 1;

                                                if (DEBUG)
                                                        System.out.println("Real move = " + moveValue + ", simu move = " + simuMove);

                                                if (simuMove == 1) {
                                                        tvSimuMove.setText("Metro moving");
                                                } else {
                                                        tvSimuMove.setText("Metro stopping");
                                                }

                                                if (moveValue == simuMove) {
                                                        // Same state
                                                        tvSameState.setText("yes");
                                                } else {
                                                        // Not the same state
                                                        tvSameState.setText("no");
                                                }

                                                tvAmplitude.setText(String.valueOf(amplitude));
                                                tvFrequency.setText(String.valueOf(frequency));
                                        }

                                });
                        } catch (IbiSensorException e) {
                        }

                }
        };

        public String[] valuesToStringArray(float[] values) {

                long timestamp = System.currentTimeMillis();

                // timestamp, 0(for stop)/1(for moving), accel_x, accel_y, accel_z, volume, decibel, frequency
                String[] stringArray = { String.valueOf(timestamp), String.valueOf(moveValue), String.valueOf(values[0]), String.valueOf(values[1]), String.valueOf(values[2]), String.valueOf(amplitude),
                                String.valueOf(decibel), String.valueOf(frequency), String.valueOf(simuMove), String.valueOf(currentOutput), };
                return stringArray;
        }

        public void createCSVWriter() {

                folder = new File(Environment.getExternalStorageDirectory() + FOLDER_NAME);

                boolean var = false;
                if (!folder.exists()) {
                        var = folder.mkdir();
                }
                if (DEBUG)
                        System.out.println("Create folder = " + var);

                try {

                        System.out.println("Write New File");
                        String format = "_ddMMyyyy_HHmmss";
                        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.FRANCE);
                        String currentDateandTime = sdf.format(new Date());
                        if (DEBUG)
                                System.out.println(VARIABLE_CSV_FILE_NAME_HEADER + currentDateandTime + CSV_FILE_NAME_EXTENSION);

                        // Use no quote character to avoid ""
                        writer = new CSVWriter(new FileWriter(Environment.getExternalStorageDirectory() + FOLDER_NAME + VARIABLE_CSV_FILE_NAME_HEADER + "_" + stTransport + currentDateandTime
                                        + CSV_FILE_NAME_EXTENSION), ',', CSVWriter.NO_QUOTE_CHARACTER);

                } catch (IOException ioException) {
                        ioException.printStackTrace();
                }

        }

        public void writeCSVFile(String[] nextLine) {

                if (isExternalStorageWritable()) {
                        if (DEBUG)
                                System.out.println("External storage writable");

                        try {
                                // Write next line
                                if (writer == null)
                                        createCSVWriter();

                                writer.writeNext(nextLine);
                                if (DEBUG)
                                        System.out.println("Finish writting external storage");
                        } catch (Exception e) {
                                e.printStackTrace();
                        }

                } else {
                        if (DEBUG)
                                System.out.println("External storage not writable");
                }

        }

        // Reference : http://developer.android.com/training/basics/data-storage/files.html
        /* Checks if external storage is available for read and write */
        public boolean isExternalStorageWritable() {
                String state = Environment.getExternalStorageState();
                if (Environment.MEDIA_MOUNTED.equals(state)) {
                        return true;
                }
                return false;
        }

        /* Checks if external storage is available to at least read */
        public boolean isExternalStorageReadable() {
                String state = Environment.getExternalStorageState();
                if (Environment.MEDIA_MOUNTED.equals(state) || Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
                        return true;
                }
                return false;
        }
}