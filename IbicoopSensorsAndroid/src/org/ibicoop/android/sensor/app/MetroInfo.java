package org.ibicoop.android.sensor.app;

public class MetroInfo {

	protected long mId;
	protected double mLongitude;
	protected double mLatitude;
	protected String mName;
	protected String mArea;
	protected String mTransport;
	
	public MetroInfo(long paramId, double paramLongitude, double paramLatitude, 
			String paramName, String paramArea, String paramTransport) {
	
		mId = paramId;
		mLongitude = paramLongitude;
		mLatitude = paramLatitude;
		mName = paramName;
		mArea = paramArea;
		mTransport = paramTransport;
	}
	

	public MetroInfo(String paramId, String paramLongitude, String paramLatitude, 
			String paramName, String paramArea, String paramTransport) {

		this(Long.parseLong(paramId), 
        		Double.parseDouble(paramLongitude),
        		Double.parseDouble(paramLatitude),
        		paramName, paramArea, paramTransport);
	}
	

	public long getId() {
		return mId;
	}
	
	public double getLongitude() {
		return mLongitude;
	}

	public double getLatitude() {
		return mLatitude;
	}
	
	public String getName() {
		return mName;
	}
	
	public String getArea() {
		return mArea;
	}
	
	public String getTransport() {
		return mTransport;
	}

	public String toString() {
		String info = "MetroInfo : Id = " + mId 
				+ ", longitude = " + mLongitude 
				+ ", latitude = " + mLatitude
				+ ", name = " + mName
				+ ", area = " + mArea
				+ ", transport = " + mTransport;
		
		return info;
	}
	
	public String[] toArrayString() {
		String[] data = {String.valueOf(mId), String.valueOf(mLongitude), String.valueOf(mLatitude),
				String.valueOf(mName), String.valueOf(mArea), String.valueOf(mTransport)};
		return data;
	}
	
	public boolean sameInfo(MetroInfo targetInfo) {
		
		if ((targetInfo.getId() == mId) 
				&& (targetInfo.getLongitude() == mLongitude)
				&& (targetInfo.getLatitude() == mLatitude)
				&& (targetInfo.getName().equals(mName))
				&& (targetInfo.getArea().equals(mArea))
				&& (targetInfo.getTransport().equals(mTransport))
				) return true;
				
	
		return false;
	}
}
