package org.ibicoop.android.sensor.app;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import android.util.Log;
import au.com.bytecode.opencsv.CSVReader;

public class MetroInfoListGetter {

	private static final String TAG = "SensorAndroid.MetroInfoListGetter";
	private static final String EXCLURE_TRANSPORT_TYPE = "bus";
	private List<MetroInfo> metroInfoList;
	private static String csvFileName;
	private static CSVReader reader;
	
	private MainActivity context;
	
	public MetroInfoListGetter(MainActivity paramContext, String paramCsvFileName) {
		context = paramContext;
		csvFileName = paramCsvFileName;
		metroInfoList = new ArrayList<MetroInfo>();
		retrieveListFromCSV();
	}
	
	protected void print(String message) {
		Log.v(TAG, message);
	}
	
	public void displayList() {

	    for (MetroInfo info : metroInfoList) {
	    	print(info.toString());
	    }
	}
	
	public ArrayList<String> getStringArrayList() {
		ArrayList<String> stringArrayList = new ArrayList<String>();
		
	    for (MetroInfo info : metroInfoList) {
	    	stringArrayList.add(info.toString());
	    }
	    
	    return stringArrayList;
	}
	
	
	public List<MetroInfo> getMetroInfoList() {
		return metroInfoList;
	}
	
	
	public void addMetroInfoInList(MetroInfo paramMetroInfo) {
		metroInfoList.add(paramMetroInfo);
	}
	
	protected void retrieveListFromCSV() {
	
		try { 
			reader = new CSVReader(new InputStreamReader(context.getAssets().open(csvFileName)), '#');
			
			List<String[]> myData = reader.readAll();
			
			int count = 0;
		    
		    for (String[] data : myData) {
	
		    	count++;
	
		    	if (!data[5].equalsIgnoreCase(EXCLURE_TRANSPORT_TYPE)) {
			    	MetroInfo metroInfo = new MetroInfo(data[0], data[1],data[2],data[3], data[4], data[5]);
			        metroInfoList.add(metroInfo);		
		    	}  	
		    }
		    
		    print("Count = " + count);
	}
	catch(FileNotFoundException fileNotFoundException) {
		fileNotFoundException.printStackTrace();
	}
	catch(IOException ioException) {
		ioException.printStackTrace();
	}
}
	

}
