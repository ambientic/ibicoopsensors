package org.ibicoop.android.sensor.impl;

import org.ibicoop.sensor.IbiSensor;
import org.ibicoop.sensor.IbiSensorListener;
import org.ibicoop.sensor.IbiSensorProperties;
import org.ibicoop.sensor.data.IbiSensorData;
import org.ibicoop.sensor.data.IbiSensorDataBluetooth;
import org.ibicoop.sensor.data.IbiSensorDataBluetoothList;
import org.ibicoop.sensor.exceptions.IbiSensorException;
import org.ibicoop.sensor.resolution.IbiSensorResolution;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

/**
 * Bluetooth sensor for Bluetooth discovering
 * 
 * @author khoo
 * 
 */
public class IbiAndroidSensorBluetooth extends IbiSensor {

        private static final String NAME = "Bluetooth";

        // Android
        private Context context;
        private BluetoothAdapter bluetoothAdapter;
        private BroadcastReceiver broadcastReceiver;
        private IntentFilter intentFilter;

        // Ibicoop
        private IbiSensorResolution ibiSensorResolution;
        private IbiSensorDataBluetoothList ibiSensorData;
        private boolean isStarted;

        //
        //
        //
        private static IbiAndroidSensorBluetooth singleton;

        private IbiAndroidSensorBluetooth(Context paramContext) {
                super();

                context = paramContext;

                bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

                // Check if device supports Bluetooth
                if (bluetoothAdapter == null) {
                        // Device does not support Bluetooth
                        System.err.println("Device does not support Bluetooth");
                        return;
                }

                try {
                        this.setProperties(new IbiSensorProperties(NAME, bluetoothAdapter.getName()));
                } catch (IbiSensorException e) {
                }
                ibiSensorData = new IbiSensorDataBluetoothList(System.currentTimeMillis());
                createFilter();
                createReceiver();
        }

        synchronized public static IbiAndroidSensorBluetooth getInstance(Context paramContext, IbiSensorResolution paramResolution, IbiSensorListener ibiSensorListener) {

                if (singleton == null) {
                        singleton = new IbiAndroidSensorBluetooth(paramContext);
                }

                singleton.ibiSensorResolution = paramResolution;
                singleton.start(ibiSensorListener);

                return singleton;
        }

        //
        //
        //
        //
        //

        /*
         * (non-Javadoc)
         * 
         * @see org.ibicoop.sensor.IbiSensor#start(org.ibicoop.sensor.IbiSensorListener)
         */
        @Override
        public void start(IbiSensorListener ibiSensorListener) {
                System.out.println("Start bluetooth sensor");

                // Enable Bluetooth
                if (!bluetoothAdapter.isEnabled()) {
                        System.err.println("Bluetooth is not enabled");
                        return;
                }

                if (!isStarted) {
                        registerReceiver();
                }

                // If device is already "discovering", cancel it
                if (bluetoothAdapter.isDiscovering()) {
                        bluetoothAdapter.cancelDiscovery();
                }

                // Start Bluetooth discovery
                boolean start = bluetoothAdapter.startDiscovery();
                if (start) {
                        System.out.println("Start discovery ok");
                } else {
                        System.err.println("Start discovery failed");
                        return;
                }

                isStarted = true;

                super.start(ibiSensorListener);
        }

        /*
         * (non-Javadoc)
         * 
         * @see org.ibicoop.sensor.IbiSensor#changeResolution(org.ibicoop.sensor.resolution.IbiSensorResolution)
         */
        @Override
        public void changeResolution(IbiSensorResolution resolution) {
                // Resolution doesn't affect bluetooth sensor
        }

        /*
         * (non-Javadoc)
         * 
         * @see org.ibicoop.sensor.IbiSensor#stop()
         */
        @Override
        public void stop() throws IbiSensorException {
                // Stop discovery
                boolean cancel = bluetoothAdapter.cancelDiscovery();
                if (cancel) {
                        System.out.println("Cancel discovery ok");
                } else {
                        System.err.println("Cancel discovery not ok");
                }

                if (isStarted) {
                        // Unregister the broacast receiver if the sensor has been started
                        if (broadcastReceiver != null)
                                context.unregisterReceiver(broadcastReceiver);
                }

                isStarted = false;

                super.stop();
        }

        /*
         * (non-Javadoc)
         * 
         * @see org.ibicoop.sensor.IbiSensor#getData()
         */
        @Override
        public IbiSensorData getData() {
                return ibiSensorData;
        }

        //
        //
        //
        //
        //

        private void createFilter() {
                intentFilter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        }

        private void createReceiver() {
                broadcastReceiver = new BroadcastReceiver() {
                        public void onReceive(Context context, Intent intent) {
                                String action = intent.getAction();
                                // When discovery finds a device
                                if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                                        System.out.println("Found new bluetooth device");

                                        // Get the BluetoothDevice object from the Intent
                                        BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                                        // Add the name and address to an array adapter to show in a ListView
                                        String deviceString = device.getName() + "\n" + device.getAddress();
                                        System.out.println(deviceString);
                                        ibiSensorData.addIbiSensorDataBluetooth(new IbiSensorDataBluetooth(device.getName(), device.getAddress(), System.currentTimeMillis()));
                                        // System.out.println("Timestamp =" + ibiSensorData.getTimeStamp());
                                }
                        }
                };
        }

        private void registerReceiver() {
                // Register receiver
                context.registerReceiver(broadcastReceiver, intentFilter); // Don't forget to unregister during onDestroy
        }

}