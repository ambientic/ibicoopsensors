package org.ibicoop.android.sensor.impl;

import org.ibicoop.sensor.IbiSensor;
import org.ibicoop.sensor.data.IbiSensorDataBoolean;
import org.ibicoop.sensor.trigger.IbiSensorDataMetroStopTrigger;
import org.ibicoop.sensor.trigger.IbiSensorDataMetroStopTriggerListener;

public class IbiAndroidSensorDataMetroStopTrigger extends IbiSensorDataMetroStopTrigger {

	public IbiAndroidSensorDataMetroStopTrigger() {}

	@Override
	public void init(IbiSensor newIbiSensor, IbiSensorDataMetroStopTriggerListener listener, int requiredStopMoreThanSecond) {
		ibiSensor = newIbiSensor;
		triggerListener = listener;
		stopMoreThanSecond = requiredStopMoreThanSecond;
		//Set metro stop trigger in metro sensor
		((IbiAndroidSensorMetro) ibiSensor).setStopTrigger(this);
	}

	@Override
	public void receiveSystemCallback(IbiSensorDataBoolean callbackData) {
		if (triggerListener != null) {
			triggerListener.ibiSensorDataMetroStopTrigger(ibiSensor, this, callbackData);
		}
	}
}
