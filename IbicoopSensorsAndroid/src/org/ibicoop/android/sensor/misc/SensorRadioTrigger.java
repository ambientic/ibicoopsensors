package org.ibicoop.android.sensor.misc;

import org.ibicoop.sensor.IbiSensor;
import org.ibicoop.sensor.data.IbiSensorData;
import org.ibicoop.sensor.exceptions.IbiSensorException;
import org.ibicoop.sensor.trigger.IbiSensorDataTrigger;
import org.ibicoop.sensor.trigger.IbiSensorDataTriggerListener;

import android.os.Handler;

public class SensorRadioTrigger extends IbiSensorDataTrigger {
	IbiSensorDataTriggerListener listener;
	IbiSensor sensor;
	SensorRadioTrigger dataTrigger;

	private int period = 1;

	private Handler feedbackHandler = null;
	private Runnable feedbackRunnable = new Runnable() {
		@Override
		public void run() {
			// Code � �x�cuter de fa�on p�riodique
			SensorRadioData sensorData;
			try {
				sensorData = (SensorRadioData) sensor.getData();
				listener.ibiSensorDataTrigger(sensor, dataTrigger, sensorData);
			} catch (IbiSensorException e) {
			}

			// call again the timer
			feedbackHandler.postDelayed(this, period * 1000);
		}
	};

	//
	//
	//

	public SensorRadioTrigger(IbiSensor sensor, IbiSensorDataTriggerListener listener) {
		super(sensor, listener);

		this.sensor = sensor;
		this.listener = listener;
		this.dataTrigger = this;

		// set feedback timer
		feedbackHandler = new Handler();
		feedbackHandler.postDelayed(feedbackRunnable, period * 1000);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.ibicoop.sensor.trigger.IbiSensorDataTrigger#checkTrigger(org.ibicoop.sensor.data.IbiSensorData)
	 */
	@Override
	public boolean checkTrigger(IbiSensorData sensorData) {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.ibicoop.sensor.trigger.IbiSensorDataTrigger#terminate()
	 */
	@Override
	public void terminate() {
		if (feedbackHandler != null)
			feedbackHandler.removeCallbacks(feedbackRunnable); // removes callback and thus loop
	}

}
