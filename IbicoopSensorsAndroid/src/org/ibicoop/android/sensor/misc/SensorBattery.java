package org.ibicoop.android.sensor.misc;

import org.ibicoop.sensor.IbiSensor;
import org.ibicoop.sensor.IbiSensorListener;
import org.ibicoop.sensor.data.IbiSensorData;
import org.ibicoop.sensor.exceptions.IbiSensorException;
import org.ibicoop.sensor.resolution.IbiSensorResolution;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.PowerManager;

public class SensorBattery extends IbiSensor {

	private Context context;

	PowerManager.WakeLock lock;

	SensorBatteryData sensorData = new SensorBatteryData();

	//
	private int level = -1;
	private boolean isCharging = false;
	private int temp = -1;

	// receiver for platform info
	BroadcastReceiver receiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {

			level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
			temp = intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, -1);
			int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
			if (status == 2)
				isCharging = true;
			else
				isCharging = false;

			sensorData.getValue().put(SensorBatteryData.CHARGING, "" + isCharging);
			sensorData.getValue().put(SensorBatteryData.TEMP, "" + temp);
			sensorData.getValue().put(SensorBatteryData.PERCENT, "" + level);
		}
	};

	//
	// Constructor
	//

	static SensorBattery singleton = null;

	private SensorBattery(Context context) {
		super();
		this.context = context;
		PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
		lock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "SensorRead");

		System.out.println("XXX SensorBattery constructor");

	}

	static public synchronized SensorBattery getInstance(Context ctxt, IbiSensorListener listener) {
		if (singleton == null)
			singleton = new SensorBattery(ctxt);

		singleton.start(listener);

		return singleton;
	}

	//
	//
	// IBISENSOR METHODS
	//
	//

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.ibicoop.sensor.IbiSensor#start(org.ibicoop.sensor.IbiSensorListener)
	 */
	public void start(IbiSensorListener listener) {
		System.out.println("XXX SensorBattery start sensor");
		restartSensorMonitoring();
		IntentFilter filter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
		context.registerReceiver(receiver, filter);
		super.start(listener);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.ibicoop.sensor.IbiSensor#close()
	 */
	@Override
	public void stop() throws IbiSensorException {
		System.out.println("XXX  SensorBattery close sensor");

		stopSensorMonitoring();
		context.unregisterReceiver(receiver);
		super.stop();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.ibicoop.sensor.IbiSensor#pause()
	 */
	@Override
	public void pause() throws IbiSensorException {
		System.out.println("XXX  SensorBattery pause sensor");
		stopSensorMonitoring();
		super.pause();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.ibicoop.sensor.IbiSensor#resume()
	 */
	@Override
	public void resume() throws IbiSensorException {
		System.out.println("XXX  SensorBattery resume sensor");
		restartSensorMonitoring();
		super.resume();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.ibicoop.sensor.IbiSensor#changeResolution(org.ibicoop.sensor.resolution.IbiSensorResolution)
	 */
	@Override
	public void changeResolution(IbiSensorResolution resolution) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.ibicoop.sensor.IbiSensor#getData()
	 */
	@Override
	public IbiSensorData getData() {
		return sensorData;
	}

	//
	//
	//
	//
	//

	/**
	 * 
	 * @param isPeriodic
	 * @param rateSec
	 * @param cb
	 */
	private void restartSensorMonitoring() {
		System.out.println("XXX  SensorBattery startMonitor");
		lock.acquire();
	}

	/**
	 * 
	 */
	private void stopSensorMonitoring() {
		try {
			lock.release();
		} catch (Exception e) {
		}
	}

	private void restarSensortListenner() {
		System.out.println("XXX  SensorBattery restartListenner");

		IntentFilter filter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
		context.unregisterReceiver(receiver);
		context.registerReceiver(receiver, filter);
	}

}