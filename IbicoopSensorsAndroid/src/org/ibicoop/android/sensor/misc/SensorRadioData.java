package org.ibicoop.android.sensor.misc;

import java.util.HashMap;

import org.ibicoop.sensor.data.IbiSensorData;
import org.ibicoop.sensor.exceptions.IbiSensorDataIncompatibleException;

public class SensorRadioData extends IbiSensorData {

	public final static String CONN_TYPE = "connectivityType";
	public final static String DATA_ACTIVITY = "dataActivity";
	public final static String DATA_CONN = "dataConnection";
	public final static String PHONE_NET_TYPE = "phoneNetworkType";
	public final static String NET_TYPE = "networkType";
	public final static String CURRENT_RX = "currentRX";
	public final static String CURRENT_TX = "currentTX";

	HashMap<String, String> sensorsData = new HashMap();

	@Override
	public HashMap<String, String> getValue() {
		return sensorsData;
	}

	@Override
	public void copy(IbiSensorData dst) {
		super.copy(dst);
		SensorRadioData localDst = (SensorRadioData) dst;
		localDst.sensorsData = new HashMap(sensorsData);
	}

	@Override
	public String toString() {
		return sensorsData.toString();
	}

	@Override
	public boolean isEqualTo(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isFresherThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {
		// TODO Auto-generated method stub
		return false;
	}
}
