package org.ibicoop.android.statespace;

public class MetroModel {
	//State space given by Matlab Simulink
	
	//Moving models
	//4 inputs: accel x, accel y, accel z, amplitude
	private static final double[][] A = {
		{0.835   ,0.2455  ,0.04301  ,0.03945},
		{0.1446   ,0.6631  ,-0.5613   ,0.3218},
		{0.3729  ,-0.2526   ,0.5167    ,0.223},
		{0.2544  ,-0.7033  ,-0.4614   ,0.2991}
	};
	
	//4 inputs: accel x, accel y, accel z, amplitude
	private static final double[][] B = {
		{0.0002272   ,0.0006687  ,-2.689e-05  ,-3.408e-06},
		{-0.004258  ,-0.0002498   ,-0.001622  ,-8.533e-06},
		{-0.00284   ,-0.000601  ,-0.0009566  ,-3.568e-06},
		{0.004028   ,-0.003893    ,0.001982    ,4.65e-05}
	};		

	//4 inputs: accel x, accel y, accel z, amplitude
	private static final double[][] C = {
		{8.386  ,-30.06   ,40.91  ,0.0736},
	};	
	
	//4 inputs: accel x, accel y, accel z, amplitude	
	private static final double[][] D = {
		{0   ,0   ,0   ,0}
	};		
	
	
	//Dimension X = 4 X 1
	private static final double[][] INITIAL_STATE = {
		{0}, {0}, {0}, {0}
	};		
	
	//Sampling period is fixed at 1 second
	public static final long TS = 1000; //1000 ms = 1 s
	
	//Default offset
	private static final double DEFAULT_OFFSET = 0.55;
	private static double TOLERANCE = 0.03;
	
	private StateSpace stateSpaceMoving;
	
	private int simuMove;
	private double currentMovingOutput = 0.0;
	private double previousMovingOutput = 0.0;
	
	private int ARRAY_SIZE = 10;
	private double[] movingArray;
	private double offset = 0.0;
	
	private static int UNKNOWN = -1;
	private static int DOWN = 0;
	private static int UP = 1;

	public MetroModel() {
		stateSpaceMoving = new StateSpace(A, B, C, D, INITIAL_STATE);
		simuMove = 0;
		//Initialize values array
		movingArray = new double[ARRAY_SIZE];
		
		for (int i = 0; i < movingArray.length; i++) {
			movingArray[i] = 0.0;
		}
	}
	
	public void updateState(double[][] newInput) {
		stateSpaceMoving.updateState(newInput);
	}
	
	public void updateState(float[] acceleration, double amplitude, float frequency) {
		double accelX = Math.abs((double) acceleration[0]);
		double accelY = Math.abs((double) acceleration[1]);
		double accelZ = Math.abs((double) acceleration[2]);		
		
		//Saturation
		if (accelX > 5.0) accelX = 5.0;
		if (accelY > 12.0) accelY = 12.0;
		if (accelZ > 12.0) accelZ = 12.0;
		
		double amplitude_filtred = amplitude;
		
		if (amplitude_filtred > 1500) amplitude_filtred = 1500.0;
		
		//4 inputs: accel x, accel y, accel z, amplitude
		double[][] newInput = {
				{accelX}, {accelY}, {accelZ},{amplitude_filtred}
		};
		
		//Update state with new input
		updateState(newInput);
		
		//Get the state space output
		currentMovingOutput = stateSpaceMoving.getOutput().get(0, 0);
		
		//Update values array
		//Move moving array to top
		for (int i = 0; i < (movingArray.length - 1); i++) {
			movingArray[i] = movingArray[i + 1];
		}
		
		movingArray[movingArray.length - 1] = currentMovingOutput;
		
		offset = DEFAULT_OFFSET;
		int state = downOrUp(movingArray);
		
		if (state == UP) {
			//Going up
			offset = 0.52;
		}
		else if (state == DOWN) {
			//Going down
			offset = 0.61;	
		}
		
		if (Math.abs(currentMovingOutput - previousMovingOutput) > TOLERANCE) {
			if (currentMovingOutput > offset) {
				simuMove = 1;
			}
			else {
				simuMove = 0;
			}
		}
		
		previousMovingOutput = currentMovingOutput;
	}

	public void resetState() {
		stateSpaceMoving.resetState();
	}
	
	public double getCurrentOutput() {
		return currentMovingOutput;
	}
	
	public double getOffset() {
		return offset;
	}
	
	public int getProcessedOutput() {
		return simuMove;
	}
	
	public double calculateMovingAverage(double values[]) {
		double sum = 0.0;
		
		for (int i = 0; i < values.length; i++) {
			sum = sum + values[i];
		}
		
		return sum/values.length;
	}
	
	public int downOrUp(double values[]) {		
		int state = UNKNOWN;
		int index = values.length - 1;
		
		if ((values[index] == values[index - 1]) 
				&& (values[index - 1] == values[index - 2]) 
				&& (values[index - 2] == values[index - 3]) 
				&& (values[index - 3] == values[index - 4])
				&& (values[index - 4] == values[index - 5])
				&& (values[index - 5] == values[index - 6])
				&& (values[index - 6] == values[index - 7])
				&& (values[index - 7] == values[index - 8])
				&& (values[index - 8] == values[index - 9])					
				) {
				//down
				state = UNKNOWN;
		} else if ((values[index] < values[index - 1]) 
				&& (values[index - 1] < values[index - 2]) 
			&& (values[index - 2] < values[index - 3]) 
			&& (values[index - 3] < values[index - 4])
			&& (values[index - 4] < values[index - 5])
			&& (values[index - 5] < values[index - 6])
			&& (values[index - 6] < values[index - 7])
			&& (values[index - 7] < values[index - 8])
			&& (values[index - 8] < values[index - 9])				
			) {
			//down
			state = DOWN;
		} else if ((values[index] > values[index - 1]) 
				&& (values[index - 1] > values[index - 2]) 
				&& (values[index - 2] > values[index - 3]) 
				&& (values[index - 3] > values[index - 4])
				&& (values[index - 4] > values[index - 5])
				&& (values[index - 5] > values[index - 6])
				&& (values[index - 6] > values[index - 7])
				&& (values[index - 7] > values[index - 8])
				&& (values[index - 8] > values[index - 9])					
				) {
			state = UP;
		}
		
		return state;
	}

}
