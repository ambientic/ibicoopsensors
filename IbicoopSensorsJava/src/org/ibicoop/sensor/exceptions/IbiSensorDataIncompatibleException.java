package org.ibicoop.sensor.exceptions;

/**
 * Exception is launched when two different data are compared
 * @author khoo
 *
 */
public class IbiSensorDataIncompatibleException extends Exception {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructor of sensor data not compatible exception
	 */
	public IbiSensorDataIncompatibleException() {
		super("IbiSensorDataIncompatibleException");
	}
}
