package org.ibicoop.sensor.unit;

/**
 * Microphone sensor unit
 * @author khoo
 *
 */
public class IbiSensorUnitMicrophone extends IbiSensorUnit {

	private IbiSensorUnit amplitudeUnit;
	private IbiSensorUnit decibelUnit;
	private IbiSensorUnit frequencyUnit;
	
	/**
	 * Constructor of default microphone sensor unit
	 */
	public IbiSensorUnitMicrophone() {
		this(IbiSensorUnitType.AMPLITUDE_UNIT, IbiSensorUnitType.DECIBEL_UNIT, IbiSensorUnitType.FREQUENCY_UNIT);
	}
	
	/**
	 * Constructor of microphone sensor unit
	 * @param amplitudeUnitType Unit for amplitude
	 * @param decibelUnitType Unit for decibel
	 * @param frequencyUnitType Unit for frequency
	 */
	public IbiSensorUnitMicrophone(IbiSensorUnitType amplitudeUnitType, IbiSensorUnitType decibelUnitType, IbiSensorUnitType frequencyUnitType) {
		super(IbiSensorUnitType.NO_UNIT);
		amplitudeUnit = new IbiSensorUnit(amplitudeUnitType);
		decibelUnit = new IbiSensorUnit(decibelUnitType);
		frequencyUnit = new IbiSensorUnit(frequencyUnitType);
	}
	
	/**
	 * Get unit of amplitude
	 * @return Sensor unit of amplitude
	 */
	public IbiSensorUnit getAmplitudeUnit() {
		return amplitudeUnit;
	}
	
	/**
	 * Get unit of amplitude in String
	 * @return String Unit of amplitude in String
	 */
	public String getAmplitudeUnitString() {
		return amplitudeUnit.toString();
	}
	
	/**
	 * Get unit of decibel
	 * @return Sensor unit of decibel
	 */
	public IbiSensorUnit getDecibelUnit() {
		return decibelUnit;
	}
	
	/**
	 * Get unit of decibel in String
	 * @return String Unit of decibel in String
	 */
	public String getDecibelUnitString() {
		return amplitudeUnit.toString();
	}
	
	/**
	 * Get unit of frequency
	 * @return Sensor unit of frequency
	 */
	public IbiSensorUnit getFrequencyUnit() {
		return frequencyUnit;
	}
	
	/**
	 * Get unit of frequency in String
	 * @return String Unit of frequency in String
	 */
	public String getFrequencyUnitString() {
		return frequencyUnit.toString();
	}	
}
