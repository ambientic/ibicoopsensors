package org.ibicoop.sensor.data;

import org.ibicoop.sensor.exceptions.IbiSensorDataIncompatibleException;
import org.ibicoop.sensor.unit.IbiSensorUnit;

/**
 * Two dimensions float sensor data
 * 
 * @author khoo
 * 
 */
public class IbiSensorDataFloat2D extends IbiSensorDataFloat1D {

	protected float y;

	/**
	 * Constructor of two dimensions sensor float data
	 * 
	 * @param x
	 *            sensor data x float value
	 * @param y
	 *            sensor data y float value
	 * @param timestamp
	 *            sensor timestamp in millisecond
	 * @param unit
	 *            sensor unit
	 */
	public IbiSensorDataFloat2D(float x, float y, long timestamp, IbiSensorUnit unit) {
		super(x, timestamp, unit);
		this.y = y;
	}

	@Override
	public Object getValue() {
		float[] floatTab = { x, y };
		return ((float[]) floatTab);
	}

	/**
	 * Get the y float value
	 * 
	 * @return y float value
	 */
	public float getY() {
		return y;
	}

	@Override
	public String toString() {
		return "x = " + x + " " + sensorUnit.toString() + " , y = " + y + " " + sensorUnit.toString() + ", timestamp = " + timestamp;
	}

	@Override
	public boolean isGreaterThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataFloat2D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		float[] otherXY = ((float[]) otherData.getValue());

		return ((x > otherXY[0]) && (y > otherXY[1]));
	}

	/**
	 * Compares if current (x, y) float value is greater than other (x, y) float value
	 * 
	 * @param otherX
	 *            Other x float value
	 * @param otherY
	 *            Other y float value
	 * @return true if current (x, y) float value is greater than other (x, y) float value, false otherwise
	 */
	public boolean isGreaterThan(float otherX, float otherY) {
		return ((x > otherX) && (y > otherY));
	}

	/**
	 * Compares if current y float value is greater than other y float value
	 * 
	 * @param otherY
	 *            Other y float value
	 * @return true if current y float value is greater than other y float value, false otherwise
	 */
	public boolean yIsGreaterThan(float otherY) {
		return (y > otherY);
	}

	@Override
	public boolean isInRange(IbiSensorData inf, IbiSensorData sup) throws IbiSensorDataIncompatibleException {

		if (!(inf instanceof IbiSensorDataFloat2D) || !(sup instanceof IbiSensorDataFloat2D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		float[] otherInfXY = ((float[]) inf.getValue());
		float[] otherSupXY = ((float[]) sup.getValue());

		return ((x >= otherInfXY[0]) && (x <= otherSupXY[0]) && (y >= otherInfXY[1]) && (y <= otherSupXY[1]));
	}

	/**
	 * Compares if the current (x, y) float value is comprised between inferior and superior bound of others (x, y) float values
	 * 
	 * @param xInf
	 *            Inferior bound of other x float value
	 * @param xSup
	 *            Superior bound of other x float value
	 * @param yInf
	 *            Inferior bound of other y float value
	 * @param ySup
	 *            Superior bound of other y float value
	 * @return true if the current (x, y) float value is comprised between inferior and superior bound of others (x, y) float values, false otherwise
	 */
	public boolean isInRange(float xInf, float yInf, float xSup, float ySup) {
		return ((x >= xInf) && (x <= xSup) && (y >= yInf) && (y <= ySup));
	}

	/**
	 * Compares if the current y float value is comprised between inferior and superior bound of others y float values
	 * 
	 * @param yInf
	 *            Inferior bound of other y float value
	 * @param ySup
	 *            Superior bound of other y float value
	 * @return true if the current y float value is comprised between inferior and superior bound of others y float values, false otherwise
	 */
	public boolean yIsInRange(float yInf, float ySup) {
		return ((y >= yInf) && (y <= ySup));
	}

	@Override
	public boolean isLessThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataFloat2D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		float[] otherXY = ((float[]) otherData.getValue());

		return ((x < otherXY[0]) && (y < otherXY[1]));
	}

	/**
	 * Compares if current (x, y) float value is less than other (x, y) float value
	 * 
	 * @param otherX
	 *            Other x float value
	 * @param otherY
	 *            Other y float value
	 * @return true if current (x, y) float value is less than other (x, y) float value, false otherwise
	 */
	public boolean isLessThan(float otherX, float otherY) {
		return ((x < otherX) && (y < otherY));
	}

	/**
	 * Compares if current y float value is less than other y float value
	 * 
	 * @param otherY
	 *            Other y float value
	 * @return true if current y float value is less than other y float value, false otherwise
	 */
	public boolean yIsLessThan(float otherY) {
		return (y < otherY);
	}

	@Override
	public boolean isEqualTo(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataFloat2D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		float[] otherXY = ((float[]) otherData.getValue());

		return ((x == otherXY[0]) && (y == otherXY[1]));
	}

	/**
	 * Compares if current (x, y) float value is equal to other (x, y) float value
	 * 
	 * @param otherX
	 *            Other x float value
	 * @param otherY
	 *            Other y float value
	 * @return true if current (x, y) float value is equal to other (x, y) float value, false otherwise
	 */
	public boolean isEqualTo(float otherX, float otherY) {
		return ((x == otherX) && (y == otherY));
	}

	/**
	 * Compares if current y float value is equal to other y float value
	 * 
	 * @param otherY
	 *            Other y float value
	 * @return true if current y float value is equal to other y float value, false otherwise
	 */
	public boolean yIsEqualTo(float otherY) {
		return (y == otherY);
	}

	@Override
	public boolean isFresherThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataFloat2D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		return (timestamp > otherData.timestamp);
	}
}
