package org.ibicoop.sensor.data;

import org.ibicoop.sensor.exceptions.IbiSensorDataIncompatibleException;
import org.ibicoop.sensor.unit.IbiSensorUnit;

/**
 * Sensor data for Bluetooth which includes device name and address
 * 
 * @author khoo
 * 
 */
public class IbiSensorDataBluetooth extends IbiSensorData {

	private String name;
	private String address;

	/**
	 * Constructor of Bluetooth data
	 * 
	 * @param name
	 *            Device name
	 * @param address
	 *            Device MAC address
	 */
	public IbiSensorDataBluetooth(String name, String address, long timestamp) {
		super(timestamp, new IbiSensorUnit());
		this.name = name;
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public String getAddress() {
		return address;
	}

	@Override
	public Object getValue() {
		String[] values = { name, address };
		return ((String[]) values);
	}

	@Override
	public String toString() {
		return "name = " + name + ", " + "address = " + address;
	}

	@Override
	public boolean isEqualTo(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataBluetooth)) {
			throw new IbiSensorDataIncompatibleException();
		}

		IbiSensorDataBluetooth bluetoothData = ((IbiSensorDataBluetooth) otherData);

		return ((name == bluetoothData.getName()) && (address == bluetoothData.getAddress()));
	}

	@Override
	public boolean isFresherThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataBluetooth)) {
			throw new IbiSensorDataIncompatibleException();
		}

		return (timestamp > otherData.timestamp);
	}

}
