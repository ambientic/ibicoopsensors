package org.ibicoop.sensor.data;

import java.util.Map;

import org.ibicoop.sensor.exceptions.IbiSensorDataIncompatibleException;
import org.ibicoop.sensor.unit.IbiSensorUnitLocation;

/**
 * Location sensor data which includes latitude, longitude, altitude, radius, bearing and speed
 * 
 * @author khoo
 * 
 */
public class IbiSensorDataLocation extends IbiSensorDataNumber {

	private double latitude;
	private double longitude;
	private double altitude;
	private float radius;
	private float bearing;
	private float speed;
	private IbiSensorUnitLocation unitLocation;
	private Map<String, String> extraParameters;

	/**
	 * Constructor of location sensor data where radius of region monitoring is zero
	 * 
	 * @param latitude
	 *            Location latitude
	 * @param longitude
	 *            Location longitude
	 * @param altitude
	 *            Location altitude
	 * @param bearing
	 *            Navigation bearing
	 * @param speed
	 *            Navigation speed
	 * @param timestamp
	 *            Location data timestamp
	 * @param unitLocation
	 *            Location data unit
	 */
	public IbiSensorDataLocation(double latitude, double longitude, double altitude, float bearing, float speed, long timestamp, Map<String, String> extraParameters, IbiSensorUnitLocation unitLocation) {
		this(latitude, longitude, altitude, bearing, speed, timestamp, 0.0f, extraParameters, unitLocation);
	}

	/**
	 * Constructor of location sensor data where radius parameter defines the radius of the region monitoring
	 * 
	 * @param latitude
	 *            Location latitude
	 * @param longitude
	 *            Location longitude
	 * @param altitude
	 *            Location altitude
	 * @param bearing
	 *            Navigation bearing
	 * @param speed
	 *            Navigation speed
	 * @param timestamp
	 *            Location data timestamp
	 * @param radius
	 *            Radius of the region monitoring
	 * @param extraParameters
	 *            Extra parameters
	 * @param unitLocation
	 *            Location data unit
	 */
	public IbiSensorDataLocation(double latitude, double longitude, double altitude, float bearing, float speed, long timestamp, float radius, Map<String, String> extraParameters,
			IbiSensorUnitLocation unitLocation) {
		super(timestamp, unitLocation);
		this.latitude = latitude;
		this.longitude = longitude;
		this.altitude = altitude;
		this.bearing = bearing;
		this.speed = speed;
		this.radius = radius;
		this.extraParameters = extraParameters;
		this.unitLocation = unitLocation;
	}

	@Override
	public Object getValue() {
		String[] values = { String.valueOf(latitude), String.valueOf(longitude), String.valueOf(altitude), String.valueOf(bearing), String.valueOf(speed) };
		return ((String[]) values);
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public double getAltitude() {
		return altitude;
	}

	public float getBearing() {
		return bearing;
	}

	public float getSpeed() {
		return speed;
	}

	public float getRadius() {
		return radius;
	}

	public Map<String, String> getExtraParameters() {
		return extraParameters;
	}

	@Override
	public String toString() {
		return "latitude = " + latitude + " " + unitLocation.getCoordinateUnitString() + ", " + "longitude = " + longitude + " " + unitLocation.getCoordinateUnitString() + ", " + "altitude = "
				+ altitude + " " + unitLocation.getAltitudeUnitString() + ", " + "bearing = " + bearing + " " + unitLocation.getBearingUnitString() + ", " + "speed = " + speed + " "
				+ unitLocation.getSpeedUnitString() + ", " + "timestamp = " + timestamp;
	}

	public boolean latitudeIsGreaterThan(IbiSensorDataLocation otherData) {
		String[] values = ((String[]) otherData.getValue());
		double otherLat = Double.parseDouble(values[0]);
		return (latitude > otherLat);
	}

	public boolean latitudeIsGreaterThan(double otherLat) {
		return (latitude > otherLat);
	}

	public boolean longitudeIsGreaterThan(IbiSensorDataLocation otherData) {
		String[] values = ((String[]) otherData.getValue());
		double otherLon = Double.parseDouble(values[1]);
		return (longitude > otherLon);
	}

	public boolean longitudeIsGreaterThan(double otherLon) {
		return (longitude > otherLon);
	}

	public boolean altitudeIsGreaterThan(IbiSensorDataLocation otherData) {
		String[] values = ((String[]) otherData.getValue());
		double otherAlt = Double.parseDouble(values[2]);
		return (altitude > otherAlt);
	}

	public boolean altitudeIsGreaterThan(double otherAlt) {
		return (altitude > otherAlt);
	}

	public boolean bearingIsGreaterThan(IbiSensorDataLocation otherData) {
		String[] values = ((String[]) otherData.getValue());
		float otherBearing = Float.parseFloat(values[3]);
		return (bearing > otherBearing);
	}

	public boolean bearingIsGreaterThan(float otherBearing) {
		return (bearing > otherBearing);
	}

	public boolean speedIsGreaterThan(IbiSensorDataLocation otherData) {
		String[] values = ((String[]) otherData.getValue());
		float otherSpeed = Float.parseFloat(values[4]);
		return (speed > otherSpeed);
	}

	public boolean speedIsGreaterThan(float otherSpeed) {
		return (speed > otherSpeed);
	}

	public boolean latitudeIsLessThan(IbiSensorDataLocation otherData) {
		String[] values = ((String[]) otherData.getValue());
		double otherLat = Double.parseDouble(values[0]);
		return (latitude < otherLat);
	}

	public boolean latitudeIsLessThan(double otherLat) {
		return (latitude < otherLat);
	}

	public boolean longitudeIsLessThan(IbiSensorDataLocation otherData) {
		String[] values = ((String[]) otherData.getValue());
		double otherLon = Double.parseDouble(values[1]);
		return (longitude < otherLon);
	}

	public boolean longitudeIsLessThan(double otherLon) {
		return (longitude < otherLon);
	}

	public boolean altitudeIsLessThan(IbiSensorDataLocation otherData) {
		String[] values = ((String[]) otherData.getValue());
		double otherAlt = Double.parseDouble(values[2]);
		return (altitude < otherAlt);
	}

	public boolean altitudeIsLessThan(double otherAlt) {
		return (altitude < otherAlt);
	}

	public boolean bearingIsLessThan(IbiSensorDataLocation otherData) {
		String[] values = ((String[]) otherData.getValue());
		float otherBearing = Float.parseFloat(values[3]);
		return (bearing < otherBearing);
	}

	public boolean bearingIsLessThan(float otherBearing) {
		return (bearing < otherBearing);
	}

	public boolean speedIsLessThan(IbiSensorDataLocation otherData) {
		String[] values = ((String[]) otherData.getValue());
		float otherSpeed = Float.parseFloat(values[4]);
		return (speed < otherSpeed);
	}

	public boolean speedIsLessThan(float otherSpeed) {
		return (speed < otherSpeed);
	}

	public boolean latitudeIsEqualto(IbiSensorDataLocation otherData) {
		String[] values = ((String[]) otherData.getValue());
		double otherLat = Double.parseDouble(values[0]);
		return (latitude == otherLat);
	}

	public boolean latitudeIsEqualto(double otherLat) {
		return (latitude == otherLat);
	}

	public boolean longitudeIsEqualTo(IbiSensorDataLocation otherData) {
		String[] values = ((String[]) otherData.getValue());
		double otherLon = Double.parseDouble(values[1]);
		return (longitude == otherLon);
	}

	public boolean longitudeIsEqualTo(double otherLon) {
		return (longitude == otherLon);
	}

	public boolean altitudeIsEqualTo(IbiSensorDataLocation otherData) {
		String[] values = ((String[]) otherData.getValue());
		double otherAlt = Double.parseDouble(values[2]);
		return (altitude == otherAlt);
	}

	public boolean altitudeIsEqualTo(double otherAlt) {
		return (altitude == otherAlt);
	}

	public boolean bearingIsEqualTo(IbiSensorDataLocation otherData) {
		String[] values = ((String[]) otherData.getValue());
		float otherBearing = Float.parseFloat(values[3]);
		return (bearing == otherBearing);
	}

	public boolean bearingIsEqualTo(float otherBearing) {
		return (bearing == otherBearing);
	}

	public boolean speedIsEqualTo(IbiSensorDataLocation otherData) {
		String[] values = ((String[]) otherData.getValue());
		float otherSpeed = Float.parseFloat(values[4]);
		return (speed == otherSpeed);
	}

	public boolean speedIsEqualTo(float otherSpeed) {
		return (speed == otherSpeed);
	}

	public boolean latitudeIsInRange(IbiSensorDataLocation inf, IbiSensorDataLocation sup) {

		String[] valuesInf = ((String[]) inf.getValue());
		double latInf = Double.parseDouble(valuesInf[0]);

		String[] valuesSup = ((String[]) sup.getValue());
		double latSup = Double.parseDouble(valuesSup[0]);

		return ((latitude >= latInf) && (latitude <= latSup));
	}

	public boolean latitudeIsInRange(double latInf, double latSup) {
		return ((latitude >= latInf) && (latitude <= latSup));
	}

	public boolean longitudeIsInRange(IbiSensorDataLocation inf, IbiSensorDataLocation sup) {

		String[] valuesInf = ((String[]) inf.getValue());
		double lonInf = Double.parseDouble(valuesInf[1]);

		String[] valuesSup = ((String[]) sup.getValue());
		double lonSup = Double.parseDouble(valuesSup[1]);

		return ((longitude >= lonInf) && (longitude <= lonSup));
	}

	public boolean longitudeIsInRange(double lonInf, double lonSup) {
		return ((longitude >= lonInf) && (longitude <= lonSup));
	}

	public boolean coordinateIsInRange(IbiSensorDataLocation inf, IbiSensorDataLocation sup) {

		String[] valuesInf = ((String[]) inf.getValue());
		double latInf = Double.parseDouble(valuesInf[0]);
		double lonInf = Double.parseDouble(valuesInf[1]);

		String[] valuesSup = ((String[]) sup.getValue());
		double latSup = Double.parseDouble(valuesSup[0]);
		double lonSup = Double.parseDouble(valuesSup[1]);

		return ((latitude >= latInf) && (latitude <= latSup) && (longitude >= lonInf) && (longitude <= lonSup));
	}

	public boolean coordinateIsInRange(double latInf, double lonInf, double latSup, double lonSup) {
		return ((latitude >= latInf) && (latitude <= latSup) && (longitude >= lonInf) && (longitude <= lonSup));
	}

	public boolean coordinateIsInRegion(IbiSensorDataLocation otherData) {

		String[] values = ((String[]) otherData.getValue());
		double lat = Double.parseDouble(values[0]);
		double lon = Double.parseDouble(values[1]);
		float radius = otherData.getRadius();

		return (distanceFrom(latitude, longitude, lat, lon) <= radius);
	}

	public boolean coordinateIsInRegion(IbiSensorDataLocation otherData, float radius) {

		String[] values = ((String[]) otherData.getValue());
		double lat = Double.parseDouble(values[0]);
		double lon = Double.parseDouble(values[1]);

		return (distanceFrom(latitude, longitude, lat, lon) <= radius);
	}

	public boolean coordinateIsInRegion(double lat, double lon, float radius) {
		return (distanceFrom(latitude, longitude, lat, lon) <= radius);
	}

	public double distanceFrom(double lat1, double lng1, double lat2, double lng2) {
		double earthRadius = 6371000;
		double dLat = Math.toRadians(lat2 - lat1);
		double dLng = Math.toRadians(lng2 - lng1);
		double sindLat = Math.sin(dLat / 2);
		double sindLng = Math.sin(dLng / 2);
		double a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2) * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double dist = earthRadius * c;

		return dist;
	}

	public boolean altitudeIsInRange(IbiSensorDataLocation inf, IbiSensorDataLocation sup) {

		String[] valuesInf = ((String[]) inf.getValue());
		double altInf = Double.parseDouble(valuesInf[2]);

		String[] valuesSup = ((String[]) sup.getValue());
		double altSup = Double.parseDouble(valuesSup[2]);

		return ((altitude >= altInf) && (altitude <= altSup));
	}

	public boolean altitudeIsInRange(double altInf, double altSup) {
		return ((altitude >= altInf) && (altitude <= altSup));
	}

	public boolean bearingIsInRange(IbiSensorDataLocation inf, IbiSensorDataLocation sup) {

		String[] valuesInf = ((String[]) inf.getValue());
		float bearingInf = Float.parseFloat(valuesInf[3]);

		String[] valuesSup = ((String[]) sup.getValue());
		float bearingSup = Float.parseFloat(valuesSup[3]);

		return ((bearing >= bearingInf) && (bearing <= bearingSup));
	}

	public boolean bearingIsInRange(float bearingInf, float bearingSup) {
		return ((bearing >= bearingInf) && (bearing <= bearingSup));
	}

	public boolean speedIsInRange(IbiSensorDataLocation inf, IbiSensorDataLocation sup) {

		String[] valuesInf = ((String[]) inf.getValue());
		float speedInf = Float.parseFloat(valuesInf[4]);

		String[] valuesSup = ((String[]) sup.getValue());
		float speedSup = Float.parseFloat(valuesSup[4]);

		return ((speed >= speedInf) && (speed <= speedSup));
	}

	public boolean speedIsInRange(float speedInf, float speedSup) {
		return ((speed >= speedInf) && (speed <= speedSup));
	}

	@Override
	public boolean isGreaterThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataLocation)) {
			throw new IbiSensorDataIncompatibleException();
		}

		String[] values = ((String[]) otherData.getValue());
		double otherLat = Double.parseDouble(values[0]);
		double otherLon = Double.parseDouble(values[1]);
		double otherAlt = Double.parseDouble(values[2]);
		float otherBearing = Float.parseFloat(values[3]);
		float otherSpeed = Float.parseFloat(values[4]);

		return ((latitude > otherLat) && (longitude > otherLon) && (altitude > otherAlt) && (bearing > otherBearing) && (speed > otherSpeed));
	}

	@Override
	public boolean isLessThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataLocation)) {
			throw new IbiSensorDataIncompatibleException();
		}

		String[] values = ((String[]) otherData.getValue());
		double otherLat = Double.parseDouble(values[0]);
		double otherLon = Double.parseDouble(values[1]);
		double otherAlt = Double.parseDouble(values[2]);
		float otherBearing = Float.parseFloat(values[3]);
		float otherSpeed = Float.parseFloat(values[4]);

		return ((latitude < otherLat) && (longitude < otherLon) && (altitude < otherAlt) && (bearing < otherBearing) && (speed < otherSpeed));
	}

	@Override
	public boolean isInRange(IbiSensorData inf, IbiSensorData sup) throws IbiSensorDataIncompatibleException {

		if (!(inf instanceof IbiSensorDataLocation) || !(sup instanceof IbiSensorDataLocation)) {
			throw new IbiSensorDataIncompatibleException();
		}

		String[] valuesInf = ((String[]) inf.getValue());
		double latInf = Double.parseDouble(valuesInf[0]);
		double lonInf = Double.parseDouble(valuesInf[1]);
		double altInf = Double.parseDouble(valuesInf[2]);
		float bearingInf = Float.parseFloat(valuesInf[3]);
		float speedInf = Float.parseFloat(valuesInf[4]);

		String[] valuesSup = ((String[]) sup.getValue());
		double latSup = Double.parseDouble(valuesSup[0]);
		double lonSup = Double.parseDouble(valuesSup[1]);
		double altSup = Double.parseDouble(valuesSup[2]);
		float bearingSup = Float.parseFloat(valuesSup[3]);
		float speedSup = Float.parseFloat(valuesSup[4]);

		return ((latitude >= latInf) && (latitude <= latSup) && (longitude >= lonInf) && (longitude <= lonSup) && (altitude >= altInf) && (altitude <= altSup) && (bearing >= bearingInf)
				&& (bearing <= bearingSup) && (speed >= speedInf) && (speed <= speedSup));
	}

	@Override
	public boolean isEqualTo(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataLocation)) {
			throw new IbiSensorDataIncompatibleException();
		}

		String[] values = ((String[]) otherData.getValue());
		double otherLat = Double.parseDouble(values[0]);
		double otherLon = Double.parseDouble(values[1]);
		double otherAlt = Double.parseDouble(values[2]);
		float otherBearing = Float.parseFloat(values[3]);
		float otherSpeed = Float.parseFloat(values[4]);

		return ((latitude == otherLat) && (longitude == otherLon) && (altitude == otherAlt) && (bearing == otherBearing) && (speed == otherSpeed));
	}

	@Override
	public boolean isFresherThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataLocation)) {
			throw new IbiSensorDataIncompatibleException();
		}

		return (timestamp > otherData.timestamp);
	}

}
