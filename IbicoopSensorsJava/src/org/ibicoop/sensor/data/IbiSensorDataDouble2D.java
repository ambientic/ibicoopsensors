package org.ibicoop.sensor.data;

import org.ibicoop.sensor.exceptions.IbiSensorDataIncompatibleException;
import org.ibicoop.sensor.unit.IbiSensorUnit;

/**
 * Two dimension sensor data of type double
 * 
 * @author khoo
 * 
 */
public class IbiSensorDataDouble2D extends IbiSensorDataDouble1D {

	protected double y;

	/**
	 * Constructor of two dimension sensor data of type double
	 * 
	 * @param x
	 *            sensor data x double value
	 * @param y
	 *            sensor data y double value
	 * @param timestamp
	 *            sensor data time stamp in millisecond
	 * @param unit
	 *            sensor unit
	 */
	public IbiSensorDataDouble2D(double x, double y, long timestamp, IbiSensorUnit unit) {
		super(x, timestamp, unit);
		this.y = y;
	}

	@Override
	public Object getValue() {
		double[] doubleTab = { x, y };
		return ((double[]) doubleTab);
	}

	/**
	 * Get the y double value
	 * 
	 * @return y double value
	 */
	public double getY() {
		return y;
	}

	@Override
	public String toString() {
		return "x = " + x + " " + sensorUnit.toString() + " , y = " + y + " " + sensorUnit.toString() + ", timestamp = " + timestamp;
	}

	@Override
	public boolean isGreaterThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataDouble2D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		double[] otherXY = ((double[]) otherData.getValue());

		return ((x > otherXY[0]) && (y > otherXY[1]));
	}

	/**
	 * Compares if current (x, y) double value is greater than other (x, y) double value
	 * 
	 * @param otherX
	 *            Other x double value
	 * @param otherY
	 *            Other y double value
	 * @return true if current (x, y) double value is greater than other (x, y) double value, false otherwise
	 */
	public boolean isGreaterThan(double otherX, double otherY) {
		return ((x > otherX) && (y > otherY));
	}

	/**
	 * Compares if current y double value is greater than other y double value
	 * 
	 * @param otherY
	 *            Other y double value
	 * @return true if current y double value is greater than other y double value, false otherwise
	 */
	public boolean yIsGreaterThan(double otherY) {
		return (y > otherY);
	}

	@Override
	public boolean isInRange(IbiSensorData inf, IbiSensorData sup) throws IbiSensorDataIncompatibleException {

		if (!(inf instanceof IbiSensorDataDouble2D) || !(sup instanceof IbiSensorDataDouble2D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		double[] otherInfXY = ((double[]) inf.getValue());
		double[] otherSupXY = ((double[]) sup.getValue());

		return ((x >= otherInfXY[0]) && (x <= otherSupXY[0]) && (y >= otherInfXY[1]) && (y <= otherSupXY[1]));
	}

	/**
	 * Compares if the current (x, y) double value is comprised between inferior and superior bound of others (x, y) double values
	 * 
	 * @param xInf
	 *            Inferior bound of other x double value
	 * @param xSup
	 *            Superior bound of other x double value
	 * @param yInf
	 *            Inferior bound of other y double value
	 * @param ySup
	 *            Superior bound of other y double value
	 * @return true if the current (x, y) double value is comprised between inferior and superior bound of others (x, y) double values, false otherwise
	 */
	public boolean isInRange(double xInf, double yInf, double xSup, double ySup) {
		return ((x >= xInf) && (x <= xSup) && (y >= yInf) && (y <= ySup));
	}

	/**
	 * Compares if the current y double value is comprised between inferior and superior bound of others y double values
	 * 
	 * @param yInf
	 *            Inferior bound of other y double value
	 * @param ySup
	 *            Superior bound of other y double value
	 * @return true if the current y double value is comprised between inferior and superior bound of others y double values, false otherwise
	 */
	public boolean yIsInRange(double yInf, double ySup) {
		return ((y >= yInf) && (y <= ySup));
	}

	@Override
	public boolean isLessThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataDouble2D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		double[] otherXY = ((double[]) otherData.getValue());

		return ((x < otherXY[0]) && (y < otherXY[1]));
	}

	/**
	 * Compares if current (x, y) double value is less than other (x, y) double value
	 * 
	 * @param otherX
	 *            Other x double value
	 * @param otherY
	 *            Other y double value
	 * @return true if current (x, y) double value is less than other (x, y) double value, false otherwise
	 */
	public boolean isLessThan(double otherX, double otherY) {
		return ((x < otherX) && (y < otherY));
	}

	/**
	 * Compares if current y double value is less than other y double value
	 * 
	 * @param otherY
	 *            Other y double value
	 * @return true if current y double value is less than other y double value, false otherwise
	 */
	public boolean yIsLessThan(double otherY) {
		return (y < otherY);
	}

	@Override
	public boolean isEqualTo(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataDouble2D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		double[] otherXY = ((double[]) otherData.getValue());

		return ((x == otherXY[0]) && (y == otherXY[1]));
	}

	/**
	 * Compares if current (x, y) double value is equal to other (x, y) double value
	 * 
	 * @param otherX
	 *            Other x double value
	 * @param otherY
	 *            Other y double value
	 * @return true if current (x, y) double value is equal to other (x, y) double value, false otherwise
	 */
	public boolean isEqualTo(double otherX, double otherY) {
		return ((x == otherX) && (y == otherY));
	}

	/**
	 * Compares if current y double value is equal to other y double value
	 * 
	 * @param otherY
	 *            Other y double value
	 * @return true if current y double value is equal to other y double value, false otherwise
	 */
	public boolean yIsEqualTo(double otherY) {
		return (y == otherY);
	}

	@Override
	public boolean isFresherThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataDouble2D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		return (timestamp > otherData.timestamp);
	}
}
