package org.ibicoop.sensor.data;

import org.ibicoop.sensor.exceptions.IbiSensorDataIncompatibleException;
import org.ibicoop.sensor.unit.IbiSensorUnit;

/**
 * Ibisensor data of type Boolean
 * 
 * @author khoo
 * 
 */
public class IbiSensorDataBoolean extends IbiSensorData {

	Boolean value;

	public IbiSensorDataBoolean(Boolean value, long timestamp) {
		super(timestamp, new IbiSensorUnit());
		this.value = value;
	}

	@Override
	public Object getValue() {
		return ((Boolean) value);
	}

	@Override
	public String toString() {
		return "value = " + value;
	}

	@Override
	public boolean isEqualTo(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataBoolean)) {
			throw new IbiSensorDataIncompatibleException();
		}

		return value == ((Boolean) otherData.getValue());
	}

	@Override
	public boolean isFresherThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataBoolean)) {
			throw new IbiSensorDataIncompatibleException();
		}

		return timestamp > otherData.getTimeStamp();
	}

}
