package org.ibicoop.sensor.data;

import org.ibicoop.sensor.exceptions.IbiSensorDataIncompatibleException;
import org.ibicoop.sensor.unit.IbiSensorUnit;
import org.ibicoop.sensor.unit.IbiSensorUnitType;

/**
 * Sensor data
 * 
 * @author khoo
 * 
 */
public abstract class IbiSensorData {

	protected long timestamp;
	protected IbiSensorUnit sensorUnit;

	/**
	 * Constructor of sensor data without timestamp and unit
	 */
	public IbiSensorData() {
		// set current time for default timestamp
		// XXX TODO change so that long time in the current timezone
		this(System.currentTimeMillis(), new IbiSensorUnit(IbiSensorUnitType.NO_UNIT));
	}

	/**
	 * Constructor of sensor data with unit but no timestamp, can be used in the case of threshold
	 */
	public IbiSensorData(IbiSensorUnit sensorUnit) {
		this(-1, sensorUnit);
	}

	/**
	 * Constructor of sensor data with timestamp and sensor unit
	 * 
	 * @param timestamp
	 * @param sensorUnit
	 */
	public IbiSensorData(long timestamp, IbiSensorUnit sensorUnit) {
		this.timestamp = timestamp;
		this.sensorUnit = sensorUnit;
	}

	/**
	 * Get timestamp of the sensor data
	 * 
	 * @return timestamp
	 */
	public long getTimeStamp() {
		return timestamp;
	}

	/**
	 * Get the sensor unit
	 * 
	 * @return sensor unit
	 */
	public IbiSensorUnit gerSensorUnit() {
		return sensorUnit;
	}

	/**
	 * Get the sensor data value
	 * 
	 * @return sensor data value
	 */
	public abstract Object getValue();

	/**
	 * Get a copy of the sensor data
	 * 
	 * @return
	 */
	public void copy(IbiSensorData dst) {
		dst.timestamp = timestamp;
		dst.sensorUnit = sensorUnit;
		return;
	}

	/**
	 * Get sensor data description
	 */
	public abstract String toString();

	/**
	 * Test if this sensor data is equal to other sensor data
	 * 
	 * @param otherData
	 *            other sensor data
	 * @return true if this sensor data is equal to other sensor data
	 * @throws IbiSensorDataIncompatibleException
	 */
	public abstract boolean isEqualTo(IbiSensorData otherData) throws IbiSensorDataIncompatibleException;

	/**
	 * Test if this sensor data is fresher than other sensor data
	 * 
	 * @param otherData
	 *            other sensor data
	 * @return true if this sensor data is fresher than sensor data
	 * @throws IbiSensorDataIncompatibleException
	 */
	public abstract boolean isFresherThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException;

}
