package org.ibicoop.sensor.data;

import org.ibicoop.sensor.exceptions.IbiSensorDataIncompatibleException;
import org.ibicoop.sensor.unit.IbiSensorUnit;

/**
 * Three dimensions sensor data of type double
 * 
 * @author khoo
 * 
 */
public class IbiSensorDataDouble3D extends IbiSensorDataDouble2D {

	protected double z;

	/**
	 * Constructor of three dimensions sensor data of type double
	 * 
	 * @param x
	 *            sensor data x double value
	 * @param y
	 *            sensor data y double value
	 * @param z
	 *            sensor data z double value
	 * @param timestamp
	 *            sensor data timestamp in millisecond
	 * @param unit
	 *            sensor data unit
	 */
	public IbiSensorDataDouble3D(double x, double y, double z, long timestamp, IbiSensorUnit unit) {
		super(x, y, timestamp, unit);
		this.z = z;
	}

	@Override
	public Object getValue() {
		double[] doubleTab = { x, y, z };
		return ((double[]) doubleTab);
	}

	public double getZ() {
		return z;
	}

	@Override
	public String toString() {
		return "x = " + x + " " + sensorUnit.toString() + " , y = " + y + " " + sensorUnit.toString() + ", z = " + z + " " + sensorUnit.toString() + ", timestamp = " + timestamp;
	}

	@Override
	public boolean isGreaterThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataDouble3D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		double[] otherXYZ = ((double[]) otherData.getValue());

		return ((x > otherXYZ[0]) && (y > otherXYZ[1]) && (z > otherXYZ[2]));
	}

	public boolean isGreaterThan(double otherX, double otherY, double otherZ) {
		return ((x > otherX) && (y > otherY) && (z > otherZ));
	}

	public boolean zIsGreaterThan(double otherZ) {
		return (z > otherZ);
	}

	@Override
	public boolean isInRange(IbiSensorData inf, IbiSensorData sup) throws IbiSensorDataIncompatibleException {

		if (!(inf instanceof IbiSensorDataDouble3D) || !(sup instanceof IbiSensorDataDouble3D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		double[] otherInfXYZ = ((double[]) inf.getValue());
		double[] otherSupXYZ = ((double[]) sup.getValue());

		return ((x >= otherInfXYZ[0]) && (x <= otherSupXYZ[0]) && (y >= otherInfXYZ[1]) && (y <= otherSupXYZ[1]) && (z >= otherInfXYZ[2]) && (z <= otherSupXYZ[2]));
	}

	public boolean isInRange(double xInf, double yInf, double zInf, double xSup, double ySup, double zSup) {

		return ((x >= xInf) && (x <= xSup) && (y >= yInf) && (y <= ySup) && (z >= zInf) && (z <= zSup));
	}

	public boolean zIsInRange(double zInf, double zSup) {

		return ((z >= zInf) && (z <= zSup));
	}

	@Override
	public boolean isLessThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataDouble3D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		double[] otherXYZ = ((double[]) otherData.getValue());

		return ((x < otherXYZ[0]) && (y < otherXYZ[1]) && (z < otherXYZ[2]));
	}

	public boolean isLessThan(double otherX, double otherY, double otherZ) {
		return ((x < otherX) && (y < otherY) && (z < otherZ));
	}

	public boolean zIsLessThan(double otherZ) {
		return (z < otherZ);
	}

	@Override
	public boolean isEqualTo(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataDouble3D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		double[] otherXYZ = ((double[]) otherData.getValue());

		return ((x == otherXYZ[0]) && (y == otherXYZ[1]) && (z == otherXYZ[2]));
	}

	public boolean isEqualTo(double otherX, double otherY, double otherZ) {
		return ((x == otherX) && (y == otherY) && (z == otherZ));
	}

	public boolean zIsEqualTo(double otherZ) {
		return (z == otherZ);
	}

	@Override
	public boolean isFresherThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataDouble3D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		return (timestamp > otherData.timestamp);
	}
}
