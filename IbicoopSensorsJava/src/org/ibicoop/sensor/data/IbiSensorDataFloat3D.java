package org.ibicoop.sensor.data;

import org.ibicoop.sensor.exceptions.IbiSensorDataIncompatibleException;
import org.ibicoop.sensor.unit.IbiSensorUnit;

/**
 * Three dimension float type sensor data
 * 
 * @author khoo
 * 
 */
public class IbiSensorDataFloat3D extends IbiSensorDataFloat2D {

	protected float z;

	/**
	 * Constructor of three dimension float type sensor data
	 * 
	 * @param x
	 *            sensor data x float value
	 * @param y
	 *            sensor data y float value
	 * @param z
	 *            sensor data z float value
	 * @param timestamp
	 *            sensor data timestamp in millisecond
	 * @param unit
	 *            sensor data unit
	 */
	public IbiSensorDataFloat3D(float x, float y, float z, long timestamp, IbiSensorUnit unit) {
		super(x, y, timestamp, unit);
		this.z = z;
	}

	@Override
	public Object getValue() {
		float[] floatTab = { x, y, z };
		return ((float[]) floatTab);
	}

	public float getZ() {
		return z;
	}

	@Override
	public String toString() {
		return "x = " + x + " " + sensorUnit.toString() + " , y = " + y + " " + sensorUnit.toString() + ", z = " + z + " " + sensorUnit.toString() + ", timestamp = " + timestamp;
	}

	@Override
	public boolean isGreaterThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataFloat3D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		float[] otherXYZ = ((float[]) otherData.getValue());

		return ((x > otherXYZ[0]) && (y > otherXYZ[1]) && (z > otherXYZ[2]));
	}

	public boolean isGreaterThan(float otherX, float otherY, float otherZ) {
		return ((x > otherX) && (y > otherY) && (z > otherZ));
	}

	public boolean zIsGreaterThan(float otherZ) {
		return (z > otherZ);
	}

	@Override
	public boolean isInRange(IbiSensorData inf, IbiSensorData sup) throws IbiSensorDataIncompatibleException {

		if (!(inf instanceof IbiSensorDataFloat3D) || !(sup instanceof IbiSensorDataFloat3D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		float[] otherInfXYZ = ((float[]) inf.getValue());
		float[] otherSupXYZ = ((float[]) sup.getValue());

		return ((x >= otherInfXYZ[0]) && (x <= otherSupXYZ[0]) && (y >= otherInfXYZ[1]) && (y <= otherSupXYZ[1]) && (z >= otherInfXYZ[2]) && (z <= otherSupXYZ[2]));
	}

	public boolean isInRange(float xInf, float yInf, float zInf, float xSup, float ySup, float zSup) {

		return ((x >= xInf) && (x <= xSup) && (y >= yInf) && (y <= ySup) && (z >= zInf) && (z <= zSup));
	}

	public boolean zIsInRange(float zInf, float zSup) {

		return ((z >= zInf) && (z <= zSup));
	}

	@Override
	public boolean isLessThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataFloat3D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		float[] otherXYZ = ((float[]) otherData.getValue());

		return ((x < otherXYZ[0]) && (y < otherXYZ[1]) && (z < otherXYZ[2]));
	}

	public boolean isLessThan(float otherX, float otherY, float otherZ) {
		return ((x < otherX) && (y < otherY) && (z < otherZ));
	}

	public boolean zIsLessThan(float otherZ) {
		return (z < otherZ);
	}

	@Override
	public boolean isEqualTo(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataFloat3D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		float[] otherXYZ = ((float[]) otherData.getValue());

		return ((x == otherXYZ[0]) && (y == otherXYZ[1]) && (z == otherXYZ[2]));
	}

	public boolean isEqualTo(float otherX, float otherY, float otherZ) {
		return ((x == otherX) && (y == otherY) && (z == otherZ));
	}

	public boolean zIsEqualTo(float otherZ) {
		return (z == otherZ);
	}

	@Override
	public boolean isFresherThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataFloat3D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		return (timestamp > otherData.timestamp);
	}
}
