package org.ibicoop.sensor.data;

import org.ibicoop.sensor.exceptions.IbiSensorDataIncompatibleException;
import org.ibicoop.sensor.unit.IbiSensorUnit;

/**
 * Two dimensions long type sensor data
 * 
 * @author khoo
 * 
 */
public class IbiSensorDataLong2D extends IbiSensorDataLong1D {

	protected long y;

	/**
	 * Constructor of two dimensions long type sensor data
	 * 
	 * @param x
	 * @param y
	 * @param timestamp
	 * @param unit
	 */
	public IbiSensorDataLong2D(long x, long y, long timestamp, IbiSensorUnit unit) {
		super(x, timestamp, unit);
		this.y = y;
	}

	@Override
	public Object getValue() {
		long[] longTab = { x, y };
		return ((long[]) longTab);
	}

	public long getY() {
		return y;
	}

	@Override
	public String toString() {
		return "x = " + x + " " + sensorUnit.toString() + " , y = " + y + " " + sensorUnit.toString() + ", timestamp = " + timestamp;
	}

	@Override
	public boolean isGreaterThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataLong2D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		long[] otherXY = ((long[]) otherData.getValue());

		return ((x > otherXY[0]) && (y > otherXY[1]));
	}

	public boolean isGreaterThan(long otherX, long otherY) {
		return ((x > otherX) && (y > otherY));
	}

	public boolean yIsGreaterThan(long otherY) {
		return (y > otherY);
	}

	@Override
	public boolean isInRange(IbiSensorData inf, IbiSensorData sup) throws IbiSensorDataIncompatibleException {

		if (!(inf instanceof IbiSensorDataLong2D) || !(sup instanceof IbiSensorDataLong2D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		long[] otherInfXY = ((long[]) inf.getValue());
		long[] otherSupXY = ((long[]) sup.getValue());

		return ((x >= otherInfXY[0]) && (x <= otherSupXY[0]) && (y >= otherInfXY[1]) && (y <= otherSupXY[1]));
	}

	public boolean isInRange(long xInf, long yInf, long xSup, long ySup) {
		return ((x >= xInf) && (x <= xSup) && (y >= yInf) && (y <= ySup));
	}

	public boolean yIsInRange(long yInf, long ySup) {
		return ((y >= yInf) && (y <= ySup));
	}

	@Override
	public boolean isLessThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataLong2D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		long[] otherXY = ((long[]) otherData.getValue());

		return ((x < otherXY[0]) && (y < otherXY[1]));
	}

	public boolean isLessThan(long otherX, long otherY) {
		return ((x < otherX) && (y < otherY));
	}

	public boolean yIsLessThan(long otherY) {
		return (y < otherY);
	}

	@Override
	public boolean isEqualTo(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataLong2D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		long[] otherXY = ((long[]) otherData.getValue());

		return ((x == otherXY[0]) && (y == otherXY[1]));
	}

	public boolean isEqualTo(long otherX, long otherY) {
		return ((x == otherX) && (y == otherY));
	}

	public boolean yIsEqualTo(long otherY) {
		return (y == otherY);
	}

	@Override
	public boolean isFresherThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataLong2D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		return (timestamp > otherData.timestamp);
	}
}
