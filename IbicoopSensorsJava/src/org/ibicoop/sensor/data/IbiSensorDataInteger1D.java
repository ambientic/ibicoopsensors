package org.ibicoop.sensor.data;

import org.ibicoop.sensor.exceptions.IbiSensorDataIncompatibleException;
import org.ibicoop.sensor.unit.IbiSensorUnit;

/**
 * One dimension integer type sensor data
 * 
 * @author khoo
 * 
 */
public class IbiSensorDataInteger1D extends IbiSensorDataNumber {

	int x;

	/**
	 * Constructor of one dimension integer type sensor data
	 * 
	 * @param x
	 *            sensor data x integer value
	 * @param timestamp
	 *            sensor data timestamp in millisecond
	 * @param unit
	 *            sensor data unit
	 */
	public IbiSensorDataInteger1D(int x, long timestamp, IbiSensorUnit unit) {
		super(timestamp, unit);
		this.x = x;
	}

	@Override
	public Object getValue() {
		int[] integerTab = { x };
		return ((int[]) integerTab);
	}

	@Override
	public String toString() {
		return "x = " + x + " " + sensorUnit.toString() + ", timestamp = " + timestamp;
	}

	/**
	 * Get the x integer value
	 * 
	 * @return x integer value
	 */
	public int getX() {
		return x;
	}

	@Override
	public boolean isGreaterThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataInteger1D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		int otherX = ((int[]) otherData.getValue())[0];

		return (x > otherX);
	}

	/**
	 * Compares if current x integer value is greater than other x integer value
	 * 
	 * @param otherX
	 *            Other x integer value
	 * @return true if current x integer value is greater than other x integer value, false otherwise
	 */
	public boolean xIsGreaterThan(int otherX) {
		return (x > otherX);
	}

	@Override
	public boolean isInRange(IbiSensorData inf, IbiSensorData sup) throws IbiSensorDataIncompatibleException {

		if (!(inf instanceof IbiSensorDataInteger1D) || !(sup instanceof IbiSensorDataInteger1D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		int infX = ((int[]) inf.getValue())[0];
		int supX = ((int[]) sup.getValue())[0];

		return ((x >= infX) && (x <= supX));
	}

	/**
	 * Compares if the current x integer value is comprised between inferior and superior bound of others x integer values
	 * 
	 * @param infX
	 *            Inferior bound of other x integer value
	 * @param supX
	 *            Superior bound of other x integer value
	 * @return true if the current x integer value is comprised between inferior and superior bound of others x integer values, false otherwise
	 */
	public boolean xIsInRange(int infX, int supX) {
		return ((x >= infX) && (x <= supX));
	}

	@Override
	public boolean isLessThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataInteger1D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		int otherX = ((int[]) otherData.getValue())[0];

		return this.x < otherX;
	}

	/**
	 * Compares if current x integer value is less than other x integer value
	 * 
	 * @param otherX
	 *            Other x integer value
	 * @return true if current x integer value is less than other x integer value, false otherwise
	 */
	public boolean xIsLessThan(int otherX) {
		return (x < otherX);
	}

	@Override
	public boolean isEqualTo(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataInteger1D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		int otherX = ((int[]) otherData.getValue())[0];

		return (x == otherX);
	}

	/**
	 * Compares if current x integer value is equal to other x integer value
	 * 
	 * @param otherX
	 *            Other x integer value
	 * @return true if current x integer value is equal to other x integer value, false otherwise
	 */
	public boolean xIsEqualTo(int otherX) {
		return (x == otherX);
	}

	@Override
	public boolean isFresherThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataInteger1D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		return (timestamp > otherData.getTimeStamp());
	}

}
