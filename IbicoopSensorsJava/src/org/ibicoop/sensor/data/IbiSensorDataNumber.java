package org.ibicoop.sensor.data;

import org.ibicoop.sensor.exceptions.IbiSensorDataIncompatibleException;
import org.ibicoop.sensor.unit.IbiSensorUnit;
import org.ibicoop.sensor.unit.IbiSensorUnitType;

/**
 * Sensor data number whose values are comparable
 * 
 * @author khoo
 * 
 */
public abstract class IbiSensorDataNumber extends IbiSensorData {

	/**
	 * Constructor of sensor data number without timestamp and unit
	 */
	public IbiSensorDataNumber() {
		this(-1, new IbiSensorUnit(IbiSensorUnitType.NO_UNIT));
	}

	/**
	 * Constructor of sensor data number with unit but no timestamp, can be used in the case of threshold
	 */
	public IbiSensorDataNumber(IbiSensorUnit sensorUnit) {
		this(-1, sensorUnit);
	}

	/**
	 * Constructor of sensor data number with timestamp and sensor unit
	 * 
	 * @param timestamp
	 * @param sensorUnit
	 */
	public IbiSensorDataNumber(long timestamp, IbiSensorUnit sensorUnit) {
		this.timestamp = timestamp;
		this.sensorUnit = sensorUnit;
	}

	/**
	 * Test if this sensor data is greater than other sensor data
	 * 
	 * @param otherData
	 *            Other sensor data
	 * @return true if this sensor data is greater than other sensor data, else false
	 * @throws IbiSensorDataIncompatibleException
	 */
	public abstract boolean isGreaterThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException;

	/**
	 * Test if this sensor data is less than other sensor data
	 * 
	 * @param otherData
	 *            Other sensor data
	 * @return true if this sensor data is less than other sensor data, else false
	 * @throws IbiSensorDataIncompatibleException
	 */
	public abstract boolean isLessThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException;

	/**
	 * Test if this sensor data is in the range of inferior bound and superior bound of other sensor data
	 * 
	 * @param inf
	 *            Inferior bound of sensor data
	 * @param sup
	 *            Superior bound of sensor data
	 * @return true if this sensor data is in the range of inf and sup, else false
	 * @throws IbiSensorDataIncompatibleException
	 */
	public abstract boolean isInRange(IbiSensorData inf, IbiSensorData sup) throws IbiSensorDataIncompatibleException;

}
