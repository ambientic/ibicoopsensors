package org.ibicoop.sensor.data;

import org.ibicoop.sensor.exceptions.IbiSensorDataIncompatibleException;
import org.ibicoop.sensor.unit.IbiSensorUnit;

/**
 * Three dimension integer type sensor data
 * 
 * @author khoo
 * 
 */
public class IbiSensorDataInteger3D extends IbiSensorDataInteger2D {

	protected int z;

	/**
	 * Constructor of three dimension integer type sensor data
	 * 
	 * @param x
	 *            sensor data x integer value
	 * @param y
	 *            sensor data y integer value
	 * @param z
	 *            sensor data z integer value
	 * @param timestamp
	 *            sensor data timestamp in millisecond
	 * @param unit
	 *            sensor data unit
	 */
	public IbiSensorDataInteger3D(int x, int y, int z, long timestamp, IbiSensorUnit unit) {
		super(x, y, timestamp, unit);
		this.z = z;
	}

	@Override
	public Object getValue() {
		int[] intTab = { x, y, z };
		return ((int[]) intTab);
	}

	public int getZ() {
		return z;
	}

	@Override
	public String toString() {
		return "x = " + x + " " + sensorUnit.toString() + " , y = " + y + " " + sensorUnit.toString() + ", z = " + z + " " + sensorUnit.toString() + ", timestamp = " + timestamp;
	}

	@Override
	public boolean isGreaterThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataInteger3D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		int[] otherXYZ = ((int[]) otherData.getValue());

		return ((x > otherXYZ[0]) && (y > otherXYZ[1]) && (z > otherXYZ[2]));
	}

	public boolean isGreaterThan(int otherX, int otherY, int otherZ) {
		return ((x > otherX) && (y > otherY) && (z > otherZ));
	}

	public boolean zIsGreaterThan(int otherZ) {
		return (z > otherZ);
	}

	@Override
	public boolean isInRange(IbiSensorData inf, IbiSensorData sup) throws IbiSensorDataIncompatibleException {

		if (!(inf instanceof IbiSensorDataInteger3D) || !(sup instanceof IbiSensorDataInteger3D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		int[] otherInfXYZ = ((int[]) inf.getValue());
		int[] otherSupXYZ = ((int[]) sup.getValue());

		return ((x >= otherInfXYZ[0]) && (x <= otherSupXYZ[0]) && (y >= otherInfXYZ[1]) && (y <= otherSupXYZ[1]) && (z >= otherInfXYZ[2]) && (z <= otherSupXYZ[2]));
	}

	public boolean isInRange(int xInf, int yInf, int zInf, int xSup, int ySup, int zSup) {

		return ((x >= xInf) && (x <= xSup) && (y >= yInf) && (y <= ySup) && (z >= zInf) && (z <= zSup));
	}

	public boolean zIsInRange(int zInf, int zSup) {

		return ((z >= zInf) && (z <= zSup));
	}

	@Override
	public boolean isLessThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataInteger3D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		int[] otherXYZ = ((int[]) otherData.getValue());

		return ((x < otherXYZ[0]) && (y < otherXYZ[1]) && (z < otherXYZ[2]));
	}

	public boolean isLessThan(int otherX, int otherY, int otherZ) {
		return ((x < otherX) && (y < otherY) && (z < otherZ));
	}

	public boolean zIsLessThan(int otherZ) {
		return (z < otherZ);
	}

	@Override
	public boolean isEqualTo(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataInteger3D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		int[] otherXYZ = ((int[]) otherData.getValue());

		return ((x == otherXYZ[0]) && (y == otherXYZ[1]) && (z == otherXYZ[2]));
	}

	public boolean isEqualTo(int otherX, int otherY, int otherZ) {
		return ((x == otherX) && (y == otherY) && (z == otherZ));
	}

	public boolean zIsEqualTo(int otherZ) {
		return (z == otherZ);
	}

	@Override
	public boolean isFresherThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataInteger3D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		return (timestamp > otherData.timestamp);
	}
}
