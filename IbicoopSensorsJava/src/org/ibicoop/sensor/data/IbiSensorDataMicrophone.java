package org.ibicoop.sensor.data;

import org.ibicoop.sensor.exceptions.IbiSensorDataIncompatibleException;
import org.ibicoop.sensor.unit.IbiSensorUnitMicrophone;

/**
 * Microphone sensor data
 * 
 * @author khoo
 * 
 */
public class IbiSensorDataMicrophone extends IbiSensorDataNumber {

	private double amplitude;
	private double maxAmplitude;
	private double minAmplitude;
	private double meanAmplitude;
	private double varianceAmplitude;
	private double standardDeviationAmplitude;
	private double range;
	private double decibel;
	private float frequency;
	private IbiSensorUnitMicrophone unitMicrophone;

	/**
	 * Constructor of microphone sensor data
	 * 
	 * @param amplitude
	 *            Sound amplitude
	 * @param maxAmplitude
	 * 				Sound maximim amplitude
	 * @param minAmplitude
	 * 				Sound minimum amplitude
	 * @param meanAmplitude
	 * 				Sound mean amplitude
	 * @param varianceAmplitude
	 * 				Sound amplitude variance
	 * @param standardDeviationAmplitude
	 * 				Sound amplitude standard deviation
	 * @param range
	 * 				Sound amplitude range
	 * @param decibel
	 *            Sound decibel
	 * @param frequency
	 *            Sound frequency
	 * @param timestamp
	 *            Microphone sensor data timestamp
	 * @param unitMicrophone
	 *            Microphone sensor data unit
	 */
	public IbiSensorDataMicrophone(
			double amplitude,
			double maxAmplitude,
			double minAmplitude,
			double meanAmplitude,
			double varianceAmplitude,
			double standardDeviationAmplitude,
			double range,
			double decibel, 
			float frequency, 
			long timestamp, 
			IbiSensorUnitMicrophone unitMicrophone) {
		this(amplitude, decibel, frequency, timestamp, unitMicrophone);
		this.maxAmplitude = maxAmplitude;
		this.minAmplitude = minAmplitude;
		this.meanAmplitude = meanAmplitude;
		this.varianceAmplitude = varianceAmplitude;
		this.standardDeviationAmplitude = standardDeviationAmplitude;
		this.range = range;
	}

	/**
	 * Constructor of microphone sensor data
	 * 
	 * @param amplitude
	 *            Sound amplitude
	 * @param decibel
	 *            Sound decibel
	 * @param frequency
	 *            Sound frequency
	 * @param timestamp
	 *            Microphone sensor data timestamp
	 * @param unitMicrophone
	 *            Microphone sensor data unit
	 */
	public IbiSensorDataMicrophone(double amplitude, double decibel, float frequency, long timestamp, IbiSensorUnitMicrophone unitMicrophone) {
		super(timestamp, unitMicrophone);
		this.amplitude = amplitude;
		this.decibel = decibel;
		this.frequency = frequency;
		this.unitMicrophone = unitMicrophone;
		//-1 to indicate no information
		this.maxAmplitude = -1;
		this.minAmplitude = -1;
		this.meanAmplitude = -1;
		this.varianceAmplitude = -1;
		this.standardDeviationAmplitude = -1;
		this.range = -1;
	}

	/**
	 * Get microphone sensor data values which include amplitude, decibel and frequency in string array
	 */
	@Override
	public Object getValue() {
		String[] values = { 
				String.valueOf(timestamp),
				String.valueOf(amplitude),
				String.valueOf(maxAmplitude),
				String.valueOf(minAmplitude),
				String.valueOf(meanAmplitude),
				String.valueOf(varianceAmplitude),
				String.valueOf(standardDeviationAmplitude),
				String.valueOf(range),
				String.valueOf(decibel), 
				String.valueOf(frequency)
				};
		return ((String[]) values);
	}

	/**
	 * Get sound amplitude
	 * 
	 * @return Sound amplitude
	 */
	public double getAmplitude() {
		return amplitude;
	}
	
	/**
	 * Get maximum amplitude
	 * @return Maximum amplitude
	 */
	public double getMaxAmplitude() {
		return maxAmplitude;
	}
	
	/**
	 * Get minimum amplitude
	 * @return Minimum amplitude
	 */
	public double getMinAmplitude() {
		return minAmplitude;
	}
	
	/**
	 * Get mean amplitude
	 * @return Mean amplitude
	 */
	public double getMeanAmplitude() {
		return meanAmplitude;
	}
	
	/**
	 * Get amplitude variance
	 * @return Amplitude variance
	 */
	public double getVarianceAmplitude() {
		return varianceAmplitude;
	}
	
	/**
	 * Get amplitude standard deviation
	 * @return Amplitude standard deviation
	 */
	public double getStandardDeviationAmplitude() {
		return standardDeviationAmplitude;
	}

	/**
	 * Get amplitude range
	 * @return Amplitude range
	 */
	public double getRange() {
		return range;
	}
	
	/**
	 * Get sound decibel
	 * 
	 * @return Sound decibel
	 */
	public double getDecibel() {
		return decibel;
	}

	/**
	 * Get sound frequency
	 * 
	 * @return Sound frequency
	 */
	public float getFrequency() {
		return frequency;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ibicoop.sensor.data.IbiSensorData#toString()
	 */
	@Override
	public String toString() {
		return "amplitude = " + String.valueOf(amplitude) + " " + unitMicrophone.getAmplitudeUnitString() + ", " +
				"Max amplitude = " + String.valueOf(maxAmplitude) + " " + unitMicrophone.getAmplitudeUnitString() + ", " +
				"Min amplitude = " + String.valueOf(minAmplitude) + " " + unitMicrophone.getAmplitudeUnitString() + ", " +
				"Mean amplitude = " + String.valueOf(meanAmplitude) + " " + unitMicrophone.getAmplitudeUnitString() + ", " +
				"Variance amplitude = " + String.valueOf(varianceAmplitude) + " " + unitMicrophone.getAmplitudeUnitString() + ", " +
				"Standard deviation amplitude = " + String.valueOf(standardDeviationAmplitude) + " " + unitMicrophone.getAmplitudeUnitString() + ", " +
				"Range = " + String.valueOf(range) + " " + unitMicrophone.getAmplitudeUnitString() + ", " +
				"" + "decibel = " + String.valueOf(decibel) + " " + unitMicrophone.getDecibelUnitString() + ", " + 
				"frequency = " + String.valueOf(frequency) + " " + unitMicrophone.getFrequencyUnitString();
	}

	/**
	 * Compare if both microhone sensor data is the same
	 * 
	 * @return true if this both microhone sensor data is the same, false otherwise
	 */
	@Override
	public boolean isEqualTo(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataMicrophone)) {
			throw new IbiSensorDataIncompatibleException();
		}

		IbiSensorDataMicrophone otherMicroData = (IbiSensorDataMicrophone) otherData;
		return ((amplitude == otherMicroData.getAmplitude()) && (decibel == otherMicroData.getDecibel()) && (frequency == otherMicroData.getFrequency()));
	}

	/**
	 * Compare if this microhone sensor data is fresher than other data
	 * 
	 * @return true if this microhone sensor data is fresher than other data, false otherwise
	 */
	@Override
	public boolean isFresherThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataMicrophone)) {
			throw new IbiSensorDataIncompatibleException();
		}

		return (timestamp > otherData.timestamp);
	}

	/**
	 * Compare if this microhone sensor data values are greater than other data
	 * 
	 * @return true if this microhone sensor data values are greater than other data, false otherwise
	 */
	@Override
	public boolean isGreaterThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {
		if (!(otherData instanceof IbiSensorDataMicrophone)) {
			throw new IbiSensorDataIncompatibleException();
		}

		IbiSensorDataMicrophone otherMicroData = (IbiSensorDataMicrophone) otherData;
		return ((amplitude > otherMicroData.getAmplitude()) && (decibel > otherMicroData.getDecibel()) && (frequency > otherMicroData.getFrequency()));
	}

	/**
	 * Compare if this amplitude is greater than other amplitude
	 * 
	 * @param otherData
	 *            Other microphone sensor data
	 * @return true if this amplitude is greater than other amplitude, false otherwise
	 */
	public boolean amplitudeIsGreaterThan(IbiSensorDataMicrophone otherData) {
		return amplitude > otherData.getAmplitude();
	}

	/**
	 * Compare if this amplitude is greater than other amplitude
	 * 
	 * @param otherData
	 *            Other microphone sensor data amplitude
	 * @return true if this amplitude is greater than other amplitude, false otherwise
	 */
	public boolean amplitudeIsGreaterThan(double otherData) {
		return amplitude > otherData;
	}

	/**
	 * Compare if this decibel is greater than other decibel
	 * 
	 * @param otherData
	 *            Other microphone sensor data
	 * @return true if this decibel is greater than other decibel, false otherwise
	 */
	public boolean decibelIsGreaterThan(IbiSensorDataMicrophone otherData) {
		return decibel > otherData.getDecibel();
	}

	/**
	 * Compare if this decibel is greater than other decibel
	 * 
	 * @param otherData
	 *            Other microphone sensor data decibel
	 * @return true of this decibel is greater than other decibel, false otherwise
	 */
	public boolean decibelIsGreaterThan(double otherData) {
		return decibel > otherData;
	}

	/**
	 * Compare if this frequency is greater than other frequency
	 * 
	 * @param otherData
	 *            Other microphone sensor data
	 * @return true if this frequency is greater than other frequency, false otherwise
	 */
	public boolean frequencyIsGreaterThan(IbiSensorDataMicrophone otherData) {
		return frequency > otherData.getDecibel();
	}

	/**
	 * Compare if this frequency is greater than other frequency
	 * 
	 * @param otherData
	 *            Other microphone sensor data frequency
	 * @return true if this frequency is greater than other frequency, false otherwise
	 */
	public boolean frequencyIsGreaterThan(double otherData) {
		return frequency > otherData;
	}

	/**
	 * Compare if this microhone sensor data values are less than other data
	 * 
	 * @return true if this microhone sensor data values are less than other data, false otherwise
	 */
	@Override
	public boolean isLessThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {
		if (!(otherData instanceof IbiSensorDataMicrophone)) {
			throw new IbiSensorDataIncompatibleException();
		}

		IbiSensorDataMicrophone otherMicroData = (IbiSensorDataMicrophone) otherData;
		return ((amplitude < otherMicroData.getAmplitude()) && (decibel < otherMicroData.getDecibel()) && (frequency < otherMicroData.getFrequency()));
	}

	/**
	 * Compare if this amplitude is less than other amplitude
	 * 
	 * @param otherData
	 *            Other microphone sensor data
	 * @return true if this amplitude is less than other amplitude, false otherwise
	 */
	public boolean amplitudeIsLessThan(IbiSensorDataMicrophone otherData) {
		return amplitude < otherData.getAmplitude();
	}

	/**
	 * Compare if this amplitude is less than other amplitude
	 * 
	 * @param otherData
	 *            Other microphone sensor data amplitude
	 * @return true if this amplitude is less than other amplitude, false otherwise
	 */
	public boolean amplitudeIsLessThan(double otherData) {
		return amplitude < otherData;
	}

	/**
	 * Compare if this decibel is less than other decibel
	 * 
	 * @param otherData
	 *            Other microphone sensor data
	 * @return true if this decibel is less than other decibel, false otherwise
	 */
	public boolean decibelIsLessThan(IbiSensorDataMicrophone otherData) {
		return decibel < otherData.getDecibel();
	}

	/**
	 * Compare if this decibel is less than other decibel
	 * 
	 * @param otherData
	 *            Other microphone sensor data decibel
	 * @return true if this decibel is less than other decibel, false otherwise
	 */
	public boolean decibelIsLessThan(double otherData) {
		return decibel < otherData;
	}

	/**
	 * Compare if this frequency is less than other frequency
	 * 
	 * @param otherData
	 *            Other microphone sensor data
	 * @return true if this frequency is less than other frequency, false otherwise
	 */
	public boolean frequencyIsLessThan(IbiSensorDataMicrophone otherData) {
		return frequency < otherData.getDecibel();
	}

	/**
	 * Compare if this frequency is less than other frequency
	 * 
	 * @param otherData
	 *            Other microphone sensor data frequency
	 * @return true if this frequency is less than other frequency, false otherwise
	 */
	public boolean frequencyIsLessThan(double otherData) {
		return frequency < otherData;
	}

	/**
	 * Compare if this microhone sensor data is in the range of inf and sup
	 * 
	 * @return true if this microhone sensor data is in the range of inf and sup, false otherwise
	 */
	@Override
	public boolean isInRange(IbiSensorData inf, IbiSensorData sup) throws IbiSensorDataIncompatibleException {
		if (!(inf instanceof IbiSensorDataMicrophone) || !(sup instanceof IbiSensorDataMicrophone)) {
			throw new IbiSensorDataIncompatibleException();
		}

		IbiSensorDataMicrophone otherMicroDataInf = (IbiSensorDataMicrophone) inf;
		IbiSensorDataMicrophone otherMicroDataSup = (IbiSensorDataMicrophone) sup;

		return ((amplitude >= otherMicroDataInf.getAmplitude()) && (amplitude <= otherMicroDataSup.getAmplitude()) && (decibel >= otherMicroDataInf.getDecibel())
				&& (decibel <= otherMicroDataSup.getDecibel()) && (frequency >= otherMicroDataInf.getFrequency()) && (frequency <= otherMicroDataSup.getFrequency()));
	}

	/**
	 * Compare if this amplitude is in the range of inf amplitude and sup amplitude
	 * 
	 * @param otherDataInf
	 *            Other microphone sensor data inf
	 * @param otherDataSup
	 *            Other microphone sensor data sup
	 * @return true if this amplitude is in the range of inf amplitude and sup amplitude, false otherwise
	 */
	public boolean amplitudeIsInRange(IbiSensorDataMicrophone otherDataInf, IbiSensorDataMicrophone otherDataSup) {
		return (amplitude >= otherDataInf.getAmplitude()) && (amplitude <= otherDataSup.getAmplitude());
	}

	/**
	 * Compare if this amplitude is in the range of inf amplitude and sup amplitude
	 * 
	 * @param otherDataInf
	 *            Other microphone sensor data inf
	 * @param otherDataSup
	 *            Other microphone sensor data sup
	 * @return true if this amplitude is in the range of inf amplitude and sup amplitude, false otherwise
	 */
	public boolean amplitudeIsInRange(double otherDataInf, double otherDataSup) {
		return (amplitude >= otherDataInf) && (amplitude <= otherDataSup);
	}

	/**
	 * Compare if this decibel is in the range of inf decibel and sup decibel
	 * 
	 * @param otherDataInf
	 *            Other microphone sensor data inf
	 * @param otherDataSup
	 *            Other microphone sensor data sup
	 * @return true if this decibel is in the range of inf decibel and sup decibel, false otherwise
	 */
	public boolean decibelIsInRange(IbiSensorDataMicrophone otherDataInf, IbiSensorDataMicrophone otherDataSup) {
		return (decibel >= otherDataInf.getDecibel()) && (decibel <= otherDataInf.getDecibel());
	}

	/**
	 * Compare if this decibel is in the range of inf decibel and sup decibel
	 * 
	 * @param otherDataInf
	 *            Other microphone sensor data inf
	 * @param otherDataSup
	 *            Other microphone sensor data sup
	 * @return true if this decibel is in the range of inf decibel and sup decibel, false otherwise
	 */
	public boolean decibelIsInRange(double otherDataInf, double otherDataSup) {
		return (decibel >= otherDataInf) && (decibel <= otherDataInf);
	}

	/**
	 * Compare if this frequency is in the range of inf frequency and sup frequency
	 * 
	 * @param otherDataInf
	 *            Other microphone sensor data inf
	 * @param otherDataSup
	 *            Other microphone sensor data sup
	 * @return true if this frequency is in the range of inf decibel and sup frequency, false otherwise
	 */
	public boolean frequencyIsInRange(IbiSensorDataMicrophone otherDataInf, IbiSensorDataMicrophone otherDataSup) {
		return (frequency >= otherDataInf.getDecibel()) && (frequency <= otherDataSup.getDecibel());
	}

	/**
	 * Compare if this frequency is in the range of inf frequency and sup frequency
	 * 
	 * @param otherDataInf
	 *            Other microphone sensor data inf
	 * @param otherDataSup
	 *            Other microphone sensor data sup
	 * @return true if this frequency is in the range of inf decibel and sup frequency, false otherwise
	 */
	public boolean frequencyIsInRange(double otherDataInf, double otherDataSup) {
		return (frequency >= otherDataInf) && (frequency <= otherDataSup);
	}

}
