package org.ibicoop.sensor.trigger;

import org.ibicoop.sensor.IbiSensor;
import org.ibicoop.sensor.data.IbiSensorData;
import org.ibicoop.sensor.data.IbiSensorDataNumber;
import org.ibicoop.sensor.exceptions.IbiSensorDataIncompatibleException;

/**
 * In range trigger with not adaptative timer which invokes the trigger when the sensor data is in the inferiour and superior bound of threshold
 * 
 * @author khoo
 * 
 */
public class IbiSensorDataTimerNotAdaptativeInRangeTrigger extends IbiSensorDataTimerNotAdaptativeTrigger {

	protected IbiSensorData thresholdInf, thresholdSup;

	/**
	 * Constructor of in range trigger with not adaptative timer. It invokes the trigger when the sensor data is in the inferiour and superior bound of threshold
	 * 
	 * @param sensor
	 *            Sensor
	 * @param thresholdInf
	 *            Inferior bound in sensor data format
	 * @param thresholdSup
	 *            Superior bound in sensor data format
	 * @param listener
	 *            Trigger listener
	 * @param period
	 *            Fixed checking period in millisecond
	 * @param timeout
	 * 			Timeout in millisecond, -1 if we don't want timeout
	 */
	public IbiSensorDataTimerNotAdaptativeInRangeTrigger(IbiSensor sensor, IbiSensorData thresholdInf, IbiSensorData thresholdSup, IbiSensorDataTriggerListener listener, long period, long timeout) {
		super(sensor, listener, period, timeout);
		this.thresholdInf = thresholdInf;
		this.thresholdSup = thresholdSup;
	}

	@Override
	public boolean checkTrigger(IbiSensorData sensorData) {

		boolean isInRange = false;

		try {
			IbiSensorDataNumber sensorDataNumber = ((IbiSensorDataNumber) sensorData);
			isInRange = sensorDataNumber.isInRange(thresholdInf, thresholdSup);
		} catch (IbiSensorDataIncompatibleException exception) {
			System.err.println(exception.getMessage());
		}

		return isInRange;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub

	}
}
