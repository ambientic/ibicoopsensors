package org.ibicoop.sensor.trigger;

import org.ibicoop.sensor.IbiSensor;
import org.ibicoop.sensor.data.IbiSensorData;
import org.ibicoop.sensor.data.IbiSensorDataLocation;

/**
 * Location in region trigger with not adaptative timer which triggers when the location is in the region. This trigger does not use the device in region monitoring system
 * 
 * @author khoo
 * 
 */
public class IbiSensorDataTimerNotAdaptativeLocationInRegionTrigger extends IbiSensorDataTimerNotAdaptativeTrigger {

	protected IbiSensorDataLocation locationThreshold;

	/**
	 * Constructor of location in region trigger with not adaptative timer
	 * 
	 * @param sensor
	 *            Sensor
	 * @param locationThreshold
	 *            Monitoring region in IbiSensorDataLocation format
	 * @param listener
	 *            Trigger listener
	 * @param period
	 *            Fixed checking period in millisecond
	 * @param timeout
	 * 				Timeout in millisecond, -1 if we don't want timeout
	 */
	public IbiSensorDataTimerNotAdaptativeLocationInRegionTrigger(IbiSensor sensor, IbiSensorDataLocation locationThreshold, IbiSensorDataTriggerListener listener, long period, long timeout) {
		super(sensor, listener, period, timeout);
		this.locationThreshold = locationThreshold;
	}

	@Override
	public boolean checkTrigger(IbiSensorData sensorData) {
		IbiSensorDataLocation locationData = ((IbiSensorDataLocation) sensorData);
		return locationData.coordinateIsInRegion(locationThreshold);
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub

	}

}
