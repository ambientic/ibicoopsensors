package org.ibicoop.sensor.trigger;

import org.ibicoop.sensor.IbiSensor;
import org.ibicoop.sensor.data.IbiSensorData;
import org.ibicoop.sensor.exceptions.IbiSensorDataIncompatibleException;

/**
 * Equal range trigger with not adaptative timer which invokes the trigger when the sensor data is equal to the threshold
 * 
 * @author khoo
 * 
 */
public class IbiSensorDataTimerNotAdaptativeEqualRangeTrigger extends IbiSensorDataTimerNotAdaptativeTrigger {

	protected IbiSensorData equalRangeThreshold;

	/**
	 * Constructor of equal range trigger with not adaptative timer
	 * 
	 * @param sensor
	 *            Sensor
	 * @param threshold
	 *            Threshold in sensor data format
	 * @param listener
	 *            Trigger listener
	 * @param period
	 *            Fixed checking period in millisecond
	 * @param timeout
	 * 				Timeout in millisecond, -1 if we don't want timeout
	 */
	public IbiSensorDataTimerNotAdaptativeEqualRangeTrigger(IbiSensor sensor, IbiSensorData threshold, IbiSensorDataTriggerListener listener, long period, long timeout) {
		super(sensor, listener, period, timeout);
		equalRangeThreshold = threshold;
	}

	@Override
	public boolean checkTrigger(IbiSensorData sensorData) {

		boolean isEqualRange = false;

		try {
			isEqualRange = sensorData.isEqualTo(equalRangeThreshold);
		} catch (IbiSensorDataIncompatibleException exception) {
			System.err.println(exception.getMessage());
		}

		return isEqualRange;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub

	}
}
