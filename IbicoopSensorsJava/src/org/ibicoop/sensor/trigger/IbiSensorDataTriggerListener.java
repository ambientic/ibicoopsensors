package org.ibicoop.sensor.trigger;

import org.ibicoop.sensor.IbiSensor;
import org.ibicoop.sensor.data.IbiSensorData;

/**
 * Trigger listener
 * @author khoo
 *
 */
public interface IbiSensorDataTriggerListener {

	/**
	 * Get the sensor data when trigger happens
	 * @param ibiSensor Sensor
	 * @param dataTrigger Trigger
	 * @param ibiSensorData Sensor data
	 */
	public void ibiSensorDataTrigger(IbiSensor ibiSensor, IbiSensorDataTrigger dataTrigger, IbiSensorData ibiSensorData);
}
