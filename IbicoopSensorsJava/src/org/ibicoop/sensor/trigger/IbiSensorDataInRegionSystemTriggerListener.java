package org.ibicoop.sensor.trigger;

import org.ibicoop.sensor.IbiSensor;
import org.ibicoop.sensor.data.IbiSensorDataLocation;

public interface IbiSensorDataInRegionSystemTriggerListener {
	/**
	 * Get the sensor data when in region trigger happens
	 * @param ibiSensor Sensor
	 * @param dataTrigger In region system trigger
	 * @param ibiSensorData Sensor data location
	 */
	public void ibiSensorDataInRegionTrigger(IbiSensor ibiSensor, IbiSensorDataInRegionSystemTrigger dataTrigger, IbiSensorDataLocation ibiSensorData);
	
	
	/**
	 * Get the sensor data when out of region trigger happens
	 * @param ibiSensor Sensor
	 * @param dataTrigger In region system trigger
	 * @param ibiSensorData Sensor data location
	 */
	public void ibiSensorDataOutOfRegionTrigger(IbiSensor ibiSensor, IbiSensorDataInRegionSystemTrigger dataTrigger, IbiSensorDataLocation ibiSensorData);
}
