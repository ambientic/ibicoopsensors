package org.ibicoop.sensor.trigger;

import org.ibicoop.sensor.IbiSensor;
import org.ibicoop.sensor.data.IbiSensorData;
import org.ibicoop.sensor.exceptions.IbiSensorDataIncompatibleException;

/**
 * Not equal range trigger with not adaptative timer which invokes the trigger when the sensor data is not equal to the threshold
 * 
 * @author khoo
 * 
 */
public class IbiSensorDataTimerNotAdaptativeNotEqualRangeTrigger extends IbiSensorDataTimerNotAdaptativeTrigger {

	protected IbiSensorData notEqualRangeThreshold;

	/**
	 * Constructor of not equal range trigger with not adaptative timer
	 * 
	 * @param sensor
	 *            Sensor
	 * @param threshold
	 *            Threshold in sensor data format
	 * @param listener
	 *            Trigger listener
	 * @param period
	 *            Fixed checking period in millisecond
	 * @param timeout
	 * 				Timeout in millisecond, -1 if we don't want timeout
	 */
	public IbiSensorDataTimerNotAdaptativeNotEqualRangeTrigger(IbiSensor sensor, IbiSensorData threshold, IbiSensorDataTriggerListener listener, long period, long timeout) {
		super(sensor, listener, period, timeout);
		notEqualRangeThreshold = threshold;
	}

	@Override
	public boolean checkTrigger(IbiSensorData sensorData) {

		boolean isNotEqualRange = false;

		try {
			isNotEqualRange = !sensorData.isEqualTo(notEqualRangeThreshold);
		} catch (IbiSensorDataIncompatibleException exception) {
			System.err.println(exception.getMessage());
		}

		return isNotEqualRange;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub

	}
}
