package org.ibicoop.sensor.trigger;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.ibicoop.sensor.IbiSensor;
import org.ibicoop.sensor.data.IbiSensorData;
import org.ibicoop.sensor.exceptions.IbiSensorException;

/**
 * Timer trigger which compares periodically the sensor data
 * and the threshold. It triggers when the sensor data meets
 * the threshold condition
 * @author khoo
 *
 */
public abstract class IbiSensorDataTimerTrigger extends IbiSensorDataTrigger {

	protected static final int CORE_POOL_SIZE = 2;
	
	protected ScheduledThreadPoolExecutor threadExecutor;
	protected ScheduledThreadPoolExecutor threadTimeoutExecutor;
	protected TimeoutTriggerThread timeoutThread;
	
	protected long checkTriggerPeriod;
	protected long triggerTimeout;
	protected boolean timeoutThreadFirstExecution = true;
	
	/**
	 * Constructor of timer trigger
	 * @param sensor Sensor
	 * @param listener Trigger listener
	 * @param period Fixed or dynamic period in ms depends on it is adaptative or not adaptative
	 * @param timeout Timeout in ms, -1 if we don't want timeout
	 */
	public IbiSensorDataTimerTrigger(IbiSensor sensor, IbiSensorDataTriggerListener listener, long period, long timeout) {
		super(sensor, listener);
		checkTriggerPeriod = period;
		triggerTimeout = timeout;
		threadExecutor = new ScheduledThreadPoolExecutor(CORE_POOL_SIZE);
		threadTimeoutExecutor = new ScheduledThreadPoolExecutor(CORE_POOL_SIZE);
		if (triggerTimeout > 0) {
			timeoutThread = new TimeoutTriggerThread();
			threadTimeoutExecutor.scheduleAtFixedRate(timeoutThread, 0, triggerTimeout, TimeUnit.MILLISECONDS);
		}
	}
	
	
	/**
	 * Stop the timer
	 */
	public void stop() {
		System.out.println("IbiSensorDataTimerTrigger :  stop trigger");
		threadExecutor.shutdownNow();
		threadTimeoutExecutor.shutdownNow();	
	}
	
	protected class TimeoutTriggerThread implements Runnable {
		@Override
		public void run() {
			if (!timeoutThreadFirstExecution) {
				System.out.println("IbiSensorDataTimerTrigger : Timeout! Going to shutdown trigger");
				stop();	
			} else {
				timeoutThreadFirstExecution = false;
			}
		}
	}

}