package org.ibicoop.sensor.trigger;

import org.ibicoop.sensor.IbiSensor;
import org.ibicoop.sensor.data.IbiSensorData;

/**
 * Trigger which invokes the trigger when the sensor data meets the threshold condition
 * 
 * @author khoo
 * 
 */
public abstract class IbiSensorDataTrigger {

	protected IbiSensor ibiSensor;
	protected IbiSensorDataTriggerListener triggerListener;

	/**
	 * Constructor of trigger
	 * 
	 * @param sensor
	 *            Sensor
	 * @param listener
	 *            Trigger listener
	 */
	public IbiSensorDataTrigger(IbiSensor sensor, IbiSensorDataTriggerListener listener) {
		ibiSensor = sensor;
		triggerListener = listener;
	}

	/**
	 * Check if the sensor data meets threshold condition
	 * 
	 * @param sensorData
	 *            Sensor data
	 * @return true if the sensor data meets the threshold condition, false otherwise
	 */
	public abstract boolean checkTrigger(IbiSensorData sensorData);

	/**
	 * Stop any activioty in the trigger
	 * 
	 */
	public abstract void terminate();
}
