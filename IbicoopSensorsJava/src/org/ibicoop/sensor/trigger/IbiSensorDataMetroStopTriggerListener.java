package org.ibicoop.sensor.trigger;

import org.ibicoop.sensor.IbiSensor;
import org.ibicoop.sensor.data.IbiSensorDataBoolean;

/**
 * IbiSensorDataMetroStopTriggerListener
 * @author khoo
 *
 */
public interface IbiSensorDataMetroStopTriggerListener {

	/**
	 * Trigger when metro stops more than the stop second
	 * @param ibiSensor Metro sensor 
	 * @param dataTrigger Metros stop trigger
	 * @param ibiSensorData Metro movement value of type Boolean
	 */
	public void ibiSensorDataMetroStopTrigger(IbiSensor ibiSensor, IbiSensorDataMetroStopTrigger dataTrigger, IbiSensorDataBoolean ibiSensorData);
}
