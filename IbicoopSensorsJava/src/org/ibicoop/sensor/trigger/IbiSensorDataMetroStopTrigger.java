package org.ibicoop.sensor.trigger;

import org.ibicoop.sensor.IbiSensor;
import org.ibicoop.sensor.data.IbiSensorDataBoolean;

/**
 * Trigger when metro is stop more than stop second
 * @author khoo
 *
 */
public abstract class IbiSensorDataMetroStopTrigger {
	
	protected static IbiSensorDataMetroStopTrigger singleton;
	protected IbiSensor ibiSensor;
	protected IbiSensorDataMetroStopTriggerListener triggerListener;
	//Stop time in second, default is zero
	protected int stopMoreThanSecond = 0;
	
	/**
	 * Get instance of IbiSensorDataMetroStopTrigger
	 * @return Instance of IbiSensorDataMetroStopTrigger
	 */
	public static IbiSensorDataMetroStopTrigger getInstance() {
		try {
			if (singleton == null) {
				singleton = (IbiSensorDataMetroStopTrigger) Class.forName("org.ibicoop.android.sensor.impl.IbiAndroidSensorDataMetroStopTrigger").newInstance();
			}			
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		
		return singleton;
	}
	
	/**
	 * Initialize IbiSensorDataMetroStopTrigger
	 * @param ibiSensor IbiSensor for metro
	 * @param listener IbiSensorDataMetroStopTriggerListener
	 * @param stopSecond Metro stop second
	 */
	public abstract void init(IbiSensor ibiSensor, IbiSensorDataMetroStopTriggerListener listener, int requiredStopMoreThanSecond);
	
	/**
	 * Receive system callback when metro is stop more than the stop second
	 * @param callbackData
	 */
	public abstract void receiveSystemCallback(IbiSensorDataBoolean callbackData);
	
	/**
	 * Get metro stop trigger second
	 * @return metro stop trigger second
	 */
	public int getStopMoreThanSecond() {
		return stopMoreThanSecond;
	}
}
