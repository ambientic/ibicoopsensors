package org.ibicoop.sensor.trigger;

import java.util.concurrent.TimeUnit;

import org.ibicoop.sensor.IbiSensor;
import org.ibicoop.sensor.data.IbiSensorData;
import org.ibicoop.sensor.exceptions.IbiSensorException;

/**
 * Adaptative timer trigger where the sensor data checking period is changing dynamically in function of the difference between the sensor data and the threshold. It triggers when sensor data meet the
 * threshold condition
 * 
 * @author khoo
 * 
 */
public abstract class IbiSensorDataTimerAdaptativeTrigger extends IbiSensorDataTimerTrigger {

	protected AdaptativeTriggerThread adaptativeTriggerThread;

	/**
	 * Adaptative timer trigger where the period is changing dynamically in function of the difference between the sensor data and the threshold
	 * 
	 * @param sensor
	 *            Sensor
	 * @param listener
	 *            Trigger listener
	 * @param period
	 *            Period in millisecond
	 * @param timeout
	 * 				Timeout in millisecond, -1 if we don't want timeout
	 */
	public IbiSensorDataTimerAdaptativeTrigger(IbiSensor sensor, IbiSensorDataTriggerListener listener, long period, long timeout) {
		super(sensor, listener, period, timeout);
		adaptativeTriggerThread = new AdaptativeTriggerThread();
		threadExecutor.scheduleAtFixedRate(adaptativeTriggerThread, 0, checkTriggerPeriod, TimeUnit.MILLISECONDS);
	}

	/**
	 * Generation of new period in function of the difference between the sensor data and the threshold
	 * 
	 * @return New period
	 */
	public abstract long generateNewPeriod();

	/**
	 * Reshecule the timer with the new period
	 */
	protected void rescheduleTimer() {
		// Reschedule timer
		long newPeriod = generateNewPeriod();

		if (checkTriggerPeriod != newPeriod) {
			checkTriggerPeriod = newPeriod;
			threadExecutor.scheduleAtFixedRate(adaptativeTriggerThread, 0, checkTriggerPeriod, TimeUnit.MILLISECONDS);
		}
	}

	/**
	 * Adaptative trigger thread which compares periodically the sensor data and the threshold and invokes the trigger listener when the threshold condition is meet. The checking period is changing
	 * dynamically in function of the difference between the sensor data and the threshold
	 * 
	 * @author khoo
	 * 
	 */
	protected class AdaptativeTriggerThread implements Runnable {
		@Override
		public void run() {
			IbiSensorData data = null;
			try {
				data = ibiSensor.getData();
				if (checkTrigger(data)) {
					System.out.println(".....IbiSensorDataTimerAdaptativeTrigger..... ");
					triggerListener.ibiSensorDataTrigger(ibiSensor, IbiSensorDataTimerAdaptativeTrigger.this, data);
					rescheduleTimer();
				}
			} catch (IbiSensorException e) {
			}
		}
	}
}
