package org.ibicoop.sensor.resolution;

/**
 * Sensor resolution
 * 
 * @author khoo
 * 
 */
public interface IbiSensorResolution {

	/**
	 * Get the sensor resolution
	 * 
	 * @return Sensor resolution
	 */
	public Object getValue();
}
