package org.ibicoop.sensor.resolution;

/**
 * Sensor time resolution
 * 
 * @author khoo
 * 
 */
public class IbiSensorResolutionTime implements IbiSensorResolution {

	private long timeMilli;

	/**
	 * Constructor of sensor time resolution
	 * 
	 * @param timeMilli
	 */
	public IbiSensorResolutionTime(long timeMilli) {
		this.timeMilli = timeMilli;
	}

	@Override
	public Object getValue() {
		return ((Long) timeMilli);
	}

	@Override
	public String toString() {
		return "Time = " + timeMilli + " ms";
	}
}
