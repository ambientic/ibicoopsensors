package org.ibicoop.sensor.resolution;

/**
 * Acccuracy for location sensor
 * @author khoo
 *
 */
public enum IbiSensorResolutionLocationAccuracyType {
	ACCURACY_COARSE,
	ACCURACY_FINE	
}
