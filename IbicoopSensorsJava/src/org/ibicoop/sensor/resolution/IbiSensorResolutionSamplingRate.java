package org.ibicoop.sensor.resolution;

/**
 * Sensor resolution of sampling rate for sound sensor
 * 
 * @author khoo
 * 
 */
public class IbiSensorResolutionSamplingRate implements IbiSensorResolution {

	private float samplingRateHz;

	/**
	 * Constructor of sensor sampling rate resolution for sound sensor
	 * 
	 * @param samplingRateHz
	 *            Sampling rate in Hz
	 */
	public IbiSensorResolutionSamplingRate(float samplingRateHz) {
		this.samplingRateHz = samplingRateHz;
	}

	@Override
	public Object getValue() {
		return ((Float) samplingRateHz);
	}

	@Override
	public String toString() {
		return "Sampling rate = " + samplingRateHz + " Hz";
	}

}
