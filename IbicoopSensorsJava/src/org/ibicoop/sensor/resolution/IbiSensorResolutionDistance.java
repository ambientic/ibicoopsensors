package org.ibicoop.sensor.resolution;

/**
 * Sensor distance resolution
 * 
 * @author khoo
 * 
 */
public class IbiSensorResolutionDistance implements IbiSensorResolution {

	private float distanceMeter;

	/**
	 * Constructor of sensor distance resolution
	 * 
	 * @param distanceMeter
	 *            Sensor distance resolution in meter
	 */
	public IbiSensorResolutionDistance(float distanceMeter) {
		this.distanceMeter = distanceMeter;
	}

	@Override
	public Object getValue() {
		return ((float) distanceMeter);
	}

	@Override
	public String toString() {
		return "Distance = " + distanceMeter + " m";
	}
}
