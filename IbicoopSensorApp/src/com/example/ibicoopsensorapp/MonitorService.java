package com.example.ibicoopsensorapp;

import org.ibicoop.init.IbicoopInit;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class MonitorService extends Service {

	SensorMonitor sensmon;

	@Override
	public void onCreate() {
		super.onCreate();

	}

	@Override
	public void onDestroy() {
		System.out.println("XXX SERVICE KILLED");
		sensmon.terminate();
		super.onDestroy();
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		boolean ok;
		Object[] args = new Object[4];
		args[0] = this.getApplicationContext();
		args[1] = "raverdy";
		args[2] = "nanana";
		args[3] = "device";
		try {
			ok = IbicoopInit.getInstance().start(args);
		} catch (Exception e) {
		}

		startSensors();

		return 0;
	}

	//
	//
	//
	//
	//

	public void startSensors() {
		sensmon = new SensorMonitor(this.getApplicationContext());
		sensmon.start();
	}

}
