package com.example.ibicoopsensorapp;

import org.ibicoop.sensor.IbiSensor;
import org.ibicoop.sensor.data.IbiSensorData;
import org.ibicoop.sensor.trigger.IbiSensorDataTrigger;
import org.ibicoop.sensor.trigger.IbiSensorDataTriggerListener;

public class SensorsMonitorTrigger extends IbiSensorDataTrigger {

	public SensorsMonitorTrigger(IbiSensor sensor, IbiSensorDataTriggerListener listener) {
		super(sensor, listener);
	}

	@Override
	public boolean checkTrigger(IbiSensorData sensorData) {
		return false;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub

	}

}
