package com.example.ibicoopsensorapp;

import java.util.HashMap;

import org.ibicoop.sensor.data.IbiSensorData;
import org.ibicoop.sensor.exceptions.IbiSensorDataIncompatibleException;

public class SensorsMonitorData extends IbiSensorData {

	HashMap<String, String> sensorsData = new HashMap();

	@Override
	public Object getValue() {
		return sensorsData;
	}

	@Override
	public void copy(IbiSensorData dst) {
		super.copy(dst);
		SensorsMonitorData localDst = (SensorsMonitorData) dst;
		localDst.sensorsData = new HashMap(sensorsData);
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isEqualTo(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isFresherThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {
		// TODO Auto-generated method stub
		return false;
	}

}
