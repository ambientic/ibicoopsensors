package com.example.ibicoopsensorapp;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;

public class RemoteMonitor extends Service {
	@Override
	public void onCreate() {
		super.onCreate();
	}

	@Override
	public IBinder onBind(Intent intent) {
		// Return the interface
		return mBinder;
	}

	private final IRemote.Stub mBinder = new IRemote.Stub() {
		public int getPid() {
			return 0;
		}

		public void basicTypes(int anInt, long aLong, boolean aBoolean, float aFloat, double aDouble, String aString) {
			// Does nothing
		}

		@Override
		public int add(int a, int b) throws RemoteException {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public int subtract(int a, int b) throws RemoteException {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public double multiply(int a, int b) throws RemoteException {
			// TODO Auto-generated method stub
			return 0;
		}
	};
}