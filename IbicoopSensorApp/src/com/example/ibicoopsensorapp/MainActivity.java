package com.example.ibicoopsensorapp;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.Menu;

public class MainActivity extends Activity implements ServiceConnection {

	boolean START_AS_SERVICE = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (START_AS_SERVICE) {
			startAsService();
		} else {
			startAsComponent();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onDestroy()
	 */
	public void onDestroy() {
		System.out.println("XXX ACTIVITY KILLED");

		if (START_AS_SERVICE) {
			stopAsService();
		} else {
			stopAsComponent();
		}

		super.onDestroy();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onBackPressed()
	 */
	public void onBackPressed() {
		super.onBackPressed();
	}

	//
	//
	//
	//
	//

	ServiceConnection serviceConn;

	@Override
	public void onServiceConnected(ComponentName name, IBinder service) {
		System.out.println("XXX onServiceConnected " + name);
	}

	@Override
	public void onServiceDisconnected(ComponentName name) {
		System.out.println("XXX onServiceDisconnected " + name);
	}

	private void startAsService() {
		Intent service = new Intent(MainActivity.this, MonitorService.class);
		ComponentName name = startService(service);

		System.out.println("XXX start service  " + name);

		int flags = 0;
		boolean ok = bindService(service, this, flags);

		System.out.println("XXX bind service  " + ok);
	}

	private void stopAsService() {
		// Will stop the service if exit the app, not bg the app
		// Intent name = new Intent(MainActivity.this, MonitorService.class);
		// boolean ok = stopService(name);
		// System.out.println("XXX stop service  " + ok);

		unbindService(this);
		System.out.println("XXX unbond service  ");
	}

	//
	//
	//
	//
	//

	SensorMonitor sensmon = null;

	private void startAsComponent() {
		sensmon = new SensorMonitor(this.getApplicationContext());
		sensmon.start();
	}

	private void stopAsComponent() {
		sensmon.terminate();
	}

}
