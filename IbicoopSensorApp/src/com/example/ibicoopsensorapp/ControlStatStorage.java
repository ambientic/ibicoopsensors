package com.example.ibicoopsensorapp;

import java.io.IOException;

import org.ibicoop.filemanager.FileManager;
import org.ibicoop.filemanager.FileManagerImpl;
import org.ibicoop.utils.IbiLoggerInterface;
import org.ibicoop.utils.Log;

import android.text.format.DateUtils;

public class ControlStatStorage {
	private static final IbiLoggerInterface LOG = Log.getLog(ControlStatStorage.class.getName());

	private static final String ROOT_CONTROL_LOCATION = "ISensors/" + "Controls/";
	private static final String CONTROL_STAT_FILE = "control_stats";
	private static final String CONTROL_STAT_EXT = ".csv";

	private static final String COMMA = ",";
	private static final Object ENDOFLINE = "\n";

	static ControlStatStorage singleton = null;

	protected FileManager fm = null;

	// CheckpointInfo[] checkpoints = null;

	//
	//
	//
	//
	//

	public synchronized static ControlStatStorage getInstance() {
		if (singleton == null) {
			singleton = new ControlStatStorage();
		}
		return singleton;
	}

	private ControlStatStorage() {
		this.fm = FileManagerImpl.getInstance();
		boolean ok = createDirectories();
		if (Log.DEBUG_MODE)
			LOG.debug("created badge control dirs = " + ok);
	}

	//
	//
	// Scan log management
	//
	//

	// synchronized public String getControlFileCopy(String eventId) {
	// String fileName = ROOT_CONTROL_LOCATION + eventId + "/" + CONTROL_STAT_FILE + CONTROL_STAT_EXT;
	// String copyfilename = ROOT_CONTROL_LOCATION + eventId + "/" + CONTROL_STAT_FILE + "_" + System.currentTimeMillis() + CONTROL_STAT_EXT;
	//
	// try {
	// boolean ok = fm.copyFileAppData(fileName, copyfilename);
	// if (!ok)
	// return "";
	// } catch (IOException e) {
	// return "";
	// }
	//
	// try {
	// String fullfilename = fm.getSystemPathAppData(copyfilename);
	// return fullfilename;
	// } catch (IOException e) {
	// return "";
	// }
	// }

	private String getHeadline() {
		StringBuffer res = new StringBuffer();

		// first is timestamp
		res.append("Time");

		for (String header : headers) {
			res.append(COMMA);
			res.append(header);
		}

		// last is endofline
		res.append(ENDOFLINE);

		return res.toString();
	}

	private String getStatLine() {
		StringBuffer res = new StringBuffer();

		// first is timestamp

		int flag = DateUtils.FORMAT_SHOW_YEAR | DateUtils.FORMAT_ABBREV_MONTH | DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_TIME | DateUtils.FORMAT_ABBREV_TIME;
		// make sure we remove the offset from GMT/UTC to be sure it is correct time (since the dateutils adds again the timezone offset)
		// time = time - TimeZone.getDefault().getRawOffset() + TimeZone.getTimeZone("Europe/Paris").getRawOffset();

		// String startStr = android.text.format.DateUtils.formatDateTime(UEventApp.getInstance().getApplicationContext(), System.currentTimeMillis(), flag);
		String startStr = "";

		res.append(startStr);

		for (String value : currentValues) {
			res.append(COMMA);
			res.append(value);
		}

		// last is endofline
		res.append(ENDOFLINE);

		return res.toString();
	}

	synchronized public void createStatFile() {
		try {
			// create directory
			createDirectories();
			// create file
			String fileName = ROOT_CONTROL_LOCATION + CONTROL_STAT_FILE + CONTROL_STAT_EXT;
			LOG.debug("creating/using fileName = " + fileName);

			//
			if (!fm.existsAppData(fileName)) {
				// create file if not exist and add headline
				fm.createFileAppData(fileName);
				String stringToAppend = getHeadline();
				fm.writeAppData(fileName, stringToAppend.getBytes());
			}
		} catch (IOException e) {
			return;
		}
	}

	synchronized public void writeStatLine() {
		try {
			String fileName = ROOT_CONTROL_LOCATION + CONTROL_STAT_FILE + CONTROL_STAT_EXT;

			// append the line for this scan
			String stringToAppend = getStatLine();
			LOG.debug("stat line generated = " + stringToAppend);

			fm.writeAppData(fileName, stringToAppend.getBytes());
		} catch (IOException e) {
			return;
		}
	}

	String[] headers = null;
	String[] currentValues = null;
	boolean doStatLoop = false;
	int timerLoop = 1;
	Thread threadLoop = null;

	public void updateData(String key, String value) {
		try {
			for (int x = 0; x < headers.length; x++) {
				if (headers[x].equals(key)) {
					currentValues[x] = value;
				}
			}
		} catch (Exception e) {
		}
	}

	public void startPeriodicStatFile(String[] headers, int seconds) {
		this.headers = headers;
		this.currentValues = new String[headers.length];
		this.doStatLoop = true;

		if (seconds >= 1) {
			timerLoop = timerLoop;
		} else {
			timerLoop = 1;
		}

		PeriodicStatTask loopTask = new PeriodicStatTask();
		Thread thread = new Thread(loopTask);
		thread.start();
	}

	public void stopPeriodicStatFile() {
		if (threadLoop != null) {
			threadLoop.interrupt();
			threadLoop = null;
		}
	}

	private class PeriodicStatTask implements Runnable {

		public void run() {

		}

	}

	//
	//
	//
	//
	//

	private boolean removeDirectories() {
		try {
			fm.deleteAppData(ROOT_CONTROL_LOCATION);
			return true;
		} catch (IOException e) {
			return false;
		}
	}

	private boolean createDirectories() {
		try {
			fm.createDirectoryAppData("ISensors/");
			fm.createDirectoryAppData("ISensors/" + "Controls/"); // ROOT_CONTROL_LOCATION)
		} catch (IOException e) {
			return false;
		}
		return true;
	}
}
