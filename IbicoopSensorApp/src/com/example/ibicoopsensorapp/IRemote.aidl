package com.example.ibicoopsensorapp;

/** Example service interface */
interface IRemote
{
	int add(int a, int b);
	int subtract(int a, int b);
	double multiply(int a, int b);
	  
	void basicTypes(int anInt, long aLong, boolean aBoolean, float aFloat, double aDouble, String aString);
}