package com.example.ibicoopsensorapp;

import java.util.List;

import org.ibicoop.android.sensor.misc.SensorBattery;
import org.ibicoop.android.sensor.misc.SensorBatteryData;
import org.ibicoop.android.sensor.misc.SensorBatteryTrigger;
import org.ibicoop.android.sensor.misc.SensorRadio;
import org.ibicoop.android.sensor.misc.SensorRadioData;
import org.ibicoop.android.sensor.misc.SensorRadioTrigger;
import org.ibicoop.sensor.IbiSensor;
import org.ibicoop.sensor.IbiSensorFactory;
import org.ibicoop.sensor.IbiSensorListener;
import org.ibicoop.sensor.IbiSensorType;
import org.ibicoop.sensor.data.IbiSensorData;
import org.ibicoop.sensor.exceptions.IbiSensorException;
import org.ibicoop.sensor.trigger.IbiSensorDataTrigger;
import org.ibicoop.sensor.trigger.IbiSensorDataTriggerListener;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;

public class SensorMonitor implements IbiSensorListener, IbiSensorDataTriggerListener {
	Context context;

	SensorBattery sb;
	SensorBatteryTrigger sbt;
	SensorRadio sr;
	SensorRadioTrigger srt;

	long initTimestamp;
	long currentTimestamp;

	/**
	 * 
	 * @param context
	 */
	public SensorMonitor(Context context) {
		this.context = context;

		initTimestamp = System.currentTimeMillis();

		// first record the list of sensors on the phone
		SensorManager mSensorManager = (SensorManager) context.getApplicationContext().getSystemService(Context.SENSOR_SERVICE);

		List<Sensor> sensors = mSensorManager.getSensorList(Sensor.TYPE_ALL);
		for (Sensor sensor : sensors) {
			System.out.println("Sensor " + sensor.getType() + " " + sensor.getName());
		}

	}

	/**
	 * 
	 */
	public void start() {
		try {
			//
			sb = (SensorBattery) IbiSensorFactory.getInstance().getSensor(context.getApplicationContext(), IbiSensorType.BATTERY, null, this);
			sbt = new SensorBatteryTrigger(sb, this);

			//
			sr = (SensorRadio) IbiSensorFactory.getInstance().getSensor(context.getApplicationContext(), IbiSensorType.RADIO, null, this);
			srt = new SensorRadioTrigger(sr, this);

		} catch (IbiSensorException e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 */
	public void terminate() {
		try {
			sbt.terminate();
			sb.stop();
		} catch (IbiSensorException e) {
		}
	}

	@Override
	public void ibiSensorDataTrigger(IbiSensor ibiSensor, IbiSensorDataTrigger dataTrigger, IbiSensorData ibiSensorData) {
		if (ibiSensor instanceof SensorBattery) {
			SensorBatteryData btData = (SensorBatteryData) ibiSensorData;
			System.out.println("Trigger battery received " + btData.toString());
		} else if (ibiSensor instanceof SensorRadio) {
			SensorRadioData rdData = (SensorRadioData) ibiSensorData;
			System.out.println("Trigger radio received " + rdData.toString());
		}
	}

	@Override
	public void stopped(IbiSensor ibiSensor) {
		System.out.println("Sensor stopped");
	}

	@Override
	public void started(IbiSensor ibiSensor) {
		System.out.println("Sensor started");

	}

	@Override
	public void paused(IbiSensor ibiSensor) {
		System.out.println("Sensor paused");

	}

	@Override
	public void resumed(IbiSensor ibiSensor) {
		System.out.println("Sensor resumed");

	}

}
